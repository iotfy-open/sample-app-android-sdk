package com.iotfy.sampleapp.base;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.iotfy.sampleapp.R;

public abstract class BaseActivity extends AppCompatActivity{

    private boolean visible = false;
    private Dialog dialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void displayToast(int resourceId, int displayTime) {
        Toast.makeText(
                getApplicationContext(),
                resourceId,
                displayTime
        ).show();
    }

    public void displayToast(String message, int displayTime) {
        Toast.makeText(
                getApplicationContext(),
                message,
                displayTime
        ).show();
    }

    public void displaySnackbar(String message, int displayTime) {
        Snackbar snack = Snackbar.make(findViewById(android.R.id.content), message, displayTime);
        View view = snack.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.BOTTOM;
        view.setLayoutParams(params);
        snack.setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE);
        snack.show();
    }

    public void displaySnackbar(View mainView, View anchorView ,String message, int displayTime) {
        if (visible){
            Snackbar snackbar = Snackbar.make(mainView, message, displayTime);
            snackbar.setAnchorView(anchorView);
            snackbar.show();}
    }

    public void displayErrorDialog(String message) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_custom_dialog);
        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button okbtn = dialog.findViewById(R.id.error_custom_dialog_ok_button);
        TextView msgTv = dialog.findViewById(R.id.error_custom_dialog_subtitle);
        msgTv.setText(message);
        okbtn.setOnClickListener(v -> {
            if (dialog != null && dialog.isShowing()){
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void displayAlertDialog(String title,String message) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_custom_dialog);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button okbtn = dialog.findViewById(R.id.error_custom_dialog_ok_button);
        TextView titleTv = dialog.findViewById(R.id.error_custom_dialog_title);
        TextView msgTv = dialog.findViewById(R.id.error_custom_dialog_subtitle);
        titleTv.setText(title);
        msgTv.setText(message);
        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()){
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }


    @Override
    protected void onStop() {
        if (dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
        super.onStop();
    }
}

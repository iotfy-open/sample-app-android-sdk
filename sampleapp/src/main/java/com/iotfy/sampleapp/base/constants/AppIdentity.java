package com.iotfy.sampleapp.base.constants;

public interface AppIdentity {

    String CLIENT_ID = "";
    String CLIENT_KEY = "";
}

package com.iotfy.sampleapp.base;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.iotfy.sampleapp.R;

/**
 * A base class for all the activities which need to check and obtain
 * if location permission and sharing is on. This is helpful when scanning
 * for Wifi signals nearby
 */

public abstract class PermissionsActivity extends BaseActivity {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final int LOCATION_PERMISSION_CODE = 101;
    private LocationManager locationManager;
    private Dialog dialog;

    @Override
    protected void onStart() {
        super.onStart();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // if the user has denied the permission
        if (requestCode == LOCATION_PERMISSION_CODE && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // This will evaluate to true if the user didn't check the "Never ask again" option
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showLocationPermissionRationaleDailog(false);
                } else {
                    showLocationPermissionRationaleDailog(true);
                }
            }
        } else {
            if (!isLocationProviderEnabled()) {
                askToEnableLocationSharing();
            }
        }
    }

    @TargetApi(23)
    public boolean hasLocationAccess() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @TargetApi(23)
    public void requestLocationAccess() {
        if (!hasLocationAccess()) {
            // Ask for the permission
            String[] requestedPermissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
            requestPermissions(requestedPermissions, LOCATION_PERMISSION_CODE);
        } else {
            // If permission is provided, then check if location sharing is turned on
            if (!isLocationProviderEnabled()) {
                askToEnableLocationSharing();
            }
        }
    }

    public boolean isLocationProviderEnabled() {
        if (locationManager == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            return locationManager.isLocationEnabled();
        } else {
            return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                    locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
        }
    }

    public boolean checkExternalStoragePermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(PermissionsActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void showLocationPermissionRationaleDailog(final boolean neverAsk) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.location_permission_dialog);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }
        dialog.setCancelable(false);

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button okbtn = dialog.findViewById(R.id.error_custom_dialog_ok_button);
        Button cancelBtn = dialog.findViewById(R.id.error_custom_dialog_cancel_button);
        TextView titleTv = dialog.findViewById(R.id.error_custom_dialog_title);
        TextView msgTv = dialog.findViewById(R.id.error_custom_dialog_subtitle);
        ImageView imageView = dialog.findViewById(R.id.error_custom_dialog_image);

        imageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_location, getTheme()));

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(18, 50, 18, 18);
        imageView.setLayoutParams(lp);
        titleTv.setText(R.string.app_location_perm_title);
        msgTv.setText(R.string.app_location_perm_content);
        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (neverAsk) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, LOCATION_PERMISSION_CODE);
                    displayToast(R.string.app_perm_setting, Toast.LENGTH_LONG);
                    dialog.dismiss();
                } else {
                    requestLocationAccess();
                    dialog.dismiss();
                }
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void askToEnableLocationSharing() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_location_permission);
        dialog.setCancelable(false);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button yesbtn = dialog.findViewById(R.id.dialog_location_permission_yes_btn);
        Button noBtn = dialog.findViewById(R.id.dialog_location_permission_no_btn);
        TextView titleTv = dialog.findViewById(R.id.dialog_location_permission_title);
        TextView msgTv = dialog.findViewById(R.id.dialog_location_permission_msg);

        titleTv.setText(R.string.app_location_sharing_title);
        msgTv.setText(R.string.app_location_sharing_content);
        yesbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askToEnableLocationSharing();
            }
        });
        dialog.show();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}

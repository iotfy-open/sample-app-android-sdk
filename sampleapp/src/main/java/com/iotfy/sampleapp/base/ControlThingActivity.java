package com.iotfy.sampleapp.base;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.iotfy.magic.common.model.DeviceType;
import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.magic.common.model.IoTfyDeviceState;
import com.iotfy.magic.common.model.StateReport;
import com.iotfy.sampleapp.things.led.LEDDashboardFragment;
import com.iotfy.sampleapp.things.plug.PlugDashboardFragment;
import com.iotfy.sampleapp.things.purifier.WaterPurifierFragment;
import com.iotfy.sampleapp.things.switches.SwitchBoardFragment;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class ControlThingActivity extends PermissionsActivity {
    private static final String TAG = ControlThingActivity.class.getSimpleName();

    private WifiManager wifiManager;

    public abstract void updateSettings(final JSONObject settingsValueJson);

    public abstract StateReport getState();

    public abstract String getDisplayLabel();

    public abstract JSONObject getConfig();

    public abstract int getType();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    public WifiManager getWifiManager() {
        return wifiManager;
    }

    public boolean isConnectedToWifi(String ssid) {
        return wifiManager.getConnectionInfo().getSSID().toLowerCase().contains(ssid.toLowerCase());
    }

    public Class getDashboardFragmentClass(int thingType) {
        switch (thingType) {
            case DeviceType.LED_TYPE:
            case DeviceType.TUBELIGHT_TYPE:
            case DeviceType.DOWNLIGHT_TYPE:
            case DeviceType.LED_STRIP_TYPE:
                return LEDDashboardFragment.class;
            case DeviceType.PLUG_TYPE:
                return PlugDashboardFragment.class;
            case DeviceType.WATER_PURIFIER_TYPE:
                return WaterPurifierFragment.class;
            case DeviceType.SWITCH_SET:
                return SwitchBoardFragment.class;
            default:
                return null;
        }
    }

    public JSONObject getCurrentSettingsJson() {
        JSONObject currentSettings;

        StateReport state = getState();
        if (state == null) {
            return defaultSettings();
        }

        try {
            currentSettings = getState().getSettings();
        } catch (NullPointerException ex) {
            Log.w(TAG, ex.toString());
            return defaultSettings();
        }

        if (currentSettings.length() == 0) {
            return defaultSettings();
        }
        return currentSettings;
    }

    private JSONObject defaultSettings() {
        JSONObject settings = new JSONObject();
        try {
            settings.put("pow", 0);
        } catch (JSONException jex) {
            Log.e(TAG, jex.toString());
            return settings;
        }

        JSONObject config = getConfig();
        if (config == null) {
            return settings;
        }

        try {
            switch (getType()) {
                case DeviceType.LED_TYPE:
                case DeviceType.TUBELIGHT_TYPE:
                case DeviceType.DOWNLIGHT_TYPE:
                case DeviceType.LED_STRIP_TYPE:
                    settings.put("pow", 1);
                    settings.put("i", 100);
                    if (config.has("colorTemp")) {
                        settings.put("colorTemp", 6000);
                    } else if (config.has("color")) {
                        settings.put("color", 16711680);
                    }
                    break;
                case DeviceType.PLUG_TYPE:
                    settings.put("pow", 0);
                    break;
                case DeviceType.FAN_TYPE:
                    settings.put("pow", 1);
                    settings.put("fspd", 5);
                    break;
                case DeviceType.TOUCH_SWITCH_SET:
                case DeviceType.SWITCH_SET:
                case DeviceType.POWERSTRIP_TYPE:
                    settings.put("pow", 1);
                    break;
                default:
                    break;
            }
        } catch (JSONException jex) {
            Log.e(TAG, jex.toString());
        }
        return settings;
    }
}

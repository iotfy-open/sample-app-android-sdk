package com.iotfy.sampleapp.models.color;

import android.graphics.Color;

public class ColorEnvelope {

    private int color;
    private String hexCode;
    private int[] argb;

    ColorEnvelope(int color) {
        this.color = color;
        this.hexCode = ColorUtils.getHexCode(color);
        this.argb = ColorUtils.getColorARGB(color);
    }

    /**
     * gets envelope's color.
     *
     * @return color.
     */
    public int getColor() {
        return color;
    }

    /**
     * gets envelope's hex code value.
     *
     * @return hex code.
     */
    public String getHexCode() {
        return hexCode;
    }

    /**
     * gets envelope's argb color.
     *
     * @return argb integer array.
     */
    public int[] getRGB() {
        return argb;
    }

    public float[] getHSV(){
        float [] hsv = new float[3];
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        Color.RGBToHSV(red,green,blue,hsv);
        return hsv;
    }

}

package com.iotfy.sampleapp.models;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class FeatureConfigPower {
    public static final String POWER_TYPE = "power";
    String name;
    JSONObject config;
    public FeatureConfigPower(String name, JSONObject config) {
        this.name = name;
        this.config = config;
    }

    public String getType() throws JSONException {
        return config.getString("type");
    }

    public JSONObject getValuesJson() throws JSONException {
        return config.getJSONObject("values");
    }

    public JSONObject getdataJson() throws JSONException {
        return config.optJSONObject("data");
    }

    public JSONArray getValuesJsonArray() throws JSONException {
        return config.optJSONArray("values");
    }

    public String getLabel() throws JSONException {
        return config.getString("label");
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return "FeatureConfigPower{" +
                "name='" + name + '\'' +
                ", config=" + config.toString() +
                '}';
    }

}

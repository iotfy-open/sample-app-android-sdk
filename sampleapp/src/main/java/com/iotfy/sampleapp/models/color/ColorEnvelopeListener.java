package com.iotfy.sampleapp.models.color;

public interface ColorEnvelopeListener extends ColorPickerViewListener {
    /**
     * @param envelope {@link ColorEnvelope}
     * @param fromUser triggered by the user(true) or not(false).
     */
    void onColorSelected(ColorEnvelope envelope, boolean fromUser);

}

package com.iotfy.sampleapp.models.color;

import android.content.Context;

class SizeUtils {
    static int dp2Px(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}

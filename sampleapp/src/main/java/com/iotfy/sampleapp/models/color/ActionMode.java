package com.iotfy.sampleapp.models.color;

public enum ActionMode {
    /** invokes listener always by tapping or dragging. */
    ALWAYS,

    /** invokes listener only when finger released. */
    LAST
}

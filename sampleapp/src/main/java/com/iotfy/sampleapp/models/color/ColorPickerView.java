package com.iotfy.sampleapp.models.color;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.ControlThingActivity;
import com.iotfy.sampleapp.models.FeatureConfigColor;
import com.iotfy.sampleapp.models.FeatureConfigColorTemp;

import org.json.JSONException;
import org.json.JSONObject;

public class ColorPickerView extends FrameLayout implements LifecycleObserver {

    private static final String TAG = ColorPickerView.class.getSimpleName();
    public static ControlThingActivity parentActivity;
    public static FeatureConfigColorTemp featureConfigColorTemp;
    public static FeatureConfigColor featureConfigColor;
    private final Handler debounceHandler = new Handler();
    public ColorPickerViewListener colorListener;
    private int selectedColor;
    private Point selectedPoint;
    private ImageView palette;
    private ImageView selector;
    private Drawable paletteDrawable;
    private Drawable selectorDrawable;
    private long debounceDuration = 0;
    private float alpha_selector = 1.0f;
    private float alpha_flag = 1.0f;
    private VelocityTracker mVelocityTracker = null; // DON'T remove null | I had to set my velocitytracker
    // to null after recycling it to make it work porperly
    private boolean VISIBLE_FLAG = false;
    private int count = 0;
    private long timed = 0L;

    private String preferenceName;

    private ActionMode actionMode = ActionMode.ALWAYS;

    public ColorPickerView(Context context) {
        super(context);
    }

    public ColorPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getAttrs(attrs);
        onCreate();
    }

    public ColorPickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getAttrs(attrs);
        onCreate();
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ColorPickerView);
        try {
            if (a.hasValue(R.styleable.ColorPickerView_palette)) {
                this.paletteDrawable = a.getDrawable(R.styleable.ColorPickerView_palette);
            }
            if (a.hasValue(R.styleable.ColorPickerView_selector)) {
                this.selectorDrawable = a.getDrawable(R.styleable.ColorPickerView_selector);
            }
            if (a.hasValue(R.styleable.ColorPickerView_alpha_selector)) {
                this.alpha_selector =
                        a.getFloat(R.styleable.ColorPickerView_alpha_selector, alpha_selector);
            }
            if (a.hasValue(R.styleable.ColorPickerView_alpha_flag)) {
                this.alpha_flag = a.getFloat(R.styleable.ColorPickerView_alpha_flag, alpha_flag);
            }
            if (a.hasValue(R.styleable.ColorPickerView_actionMode)) {
                int actionMode = a.getInteger(R.styleable.ColorPickerView_actionMode, 0);
                if (actionMode == 0) {
                    this.actionMode = ActionMode.ALWAYS;
                } else if (actionMode == 1) this.actionMode = ActionMode.LAST;
            }
            if (a.hasValue(R.styleable.ColorPickerView_debounceDuration)) {
                this.debounceDuration =
                        a.getInteger(R.styleable.ColorPickerView_debounceDuration, (int) debounceDuration);
            }
            if (a.hasValue(R.styleable.ColorPickerView_preferenceName)) {
                this.preferenceName = a.getString(R.styleable.ColorPickerView_preferenceName);
            }
        } finally {
            a.recycle();
        }
    }

    private void onCreate() {

        setPadding(0, 0, 0, 0);
        palette = new ImageView(getContext());
        if (paletteDrawable != null) {
            palette.setImageDrawable(paletteDrawable);
        } else {
            palette.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.palette));
        }

        LayoutParams paletteParam =
                new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        paletteParam.gravity = Gravity.CENTER;
        addView(palette, paletteParam);

        selector = new ImageView(getContext());
        if (selectorDrawable != null) {
            selector.setImageDrawable(selectorDrawable);
        } else {
            selector.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.wheel));
        }

        LayoutParams selectorParam =
                new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        selectorParam.gravity = Gravity.CENTER;
        addView(selector, selectorParam);
        selector.setAlpha(alpha_selector);

        getViewTreeObserver()
                .addOnGlobalLayoutListener(
                        new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                onFinishInflated();
                            }
                        });
    }

    private void onFinishInflated() {
        if (getPreferenceName() != null) {
            ColorPickerPreferenceManager.getInstance(getContext()).restoreColorPickerData(this);
        } else {
            selectCenter();
        }
    }

    /**
     * initialize the {@link ColorPickerView} by {@link ColorPickerView.Builder}.
     *
     * @param builder {@link ColorPickerView.Builder}.
     */
    protected void onCreateByBuilder(Builder builder) {
        LayoutParams params =
                new LayoutParams(
                        SizeUtils.dp2Px(getContext(), builder.width),
                        SizeUtils.dp2Px(getContext(), builder.height));
        setLayoutParams(params);

        this.paletteDrawable = builder.paletteDrawable;
        this.selectorDrawable = builder.selectorDrawable;
        this.alpha_selector = builder.alpha_selector;
        this.alpha_flag = builder.alpha_flag;
        this.debounceDuration = builder.debounceDuration;
        onCreate();

        if (builder.colorPickerViewListener != null)
            setColorListener(builder.colorPickerViewListener);
        if (builder.actionMode != null) this.actionMode = builder.actionMode;
        if (builder.preferenceName != null) setPreferenceName(builder.preferenceName);
        if (builder.lifecycleOwner != null) setLifecycleOwner(builder.lifecycleOwner);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
                selector.setPressed(true);
                return onTouchReceived(event);
            default:
                selector.setPressed(false);
                return false;


        }
    }


    /**
     * notify to the other views by the onTouchEvent.
     *
     * @param event {@link MotionEvent}.
     * @return notified or not.
     */
    @MainThread
    private boolean onTouchReceived(final MotionEvent event) {
        Point snapPoint =
                PointMapper.getColorPoint(this, new Point((int) event.getX(), (int) event.getY()));
        int pixelColor = getColorFromBitmap(snapPoint.x, snapPoint.y);

        int index = event.getActionIndex();
        int pointerId = event.getPointerId(index);

        this.selectedColor = pixelColor;
        this.selectedPoint = PointMapper.getColorPoint(this, new Point(snapPoint.x, snapPoint.y));
        setCoordinate(snapPoint.x, snapPoint.y);

        this.debounceHandler.removeCallbacksAndMessages(null);
        Runnable debounceRunnable =
                () -> {

                    // USER has raised the finger and cursor is at final position
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        updateLedColor(getColor());
                        Log.d("swipe", "-------------ACTION-SWIPE-FINISHED-------------");
                        timed = 0L;
                    }

                    /* Velocity and Time MIX */
                    if (event.getAction() == MotionEvent.ACTION_MOVE ||
                            event.getAction() == MotionEvent.ACTION_DOWN) {

                        if (mVelocityTracker == null) {
                            // Retrieve a new VelocityTracker object to watch the
                            // velocity of a motion.
                            mVelocityTracker = VelocityTracker.obtain();
                        }
                        mVelocityTracker.addMovement(event);
                        mVelocityTracker.computeCurrentVelocity(1000);

                        if (Math.abs(mVelocityTracker.getXVelocity(pointerId)) > 250 ||
                                Math.abs(mVelocityTracker.getYVelocity(pointerId)) > 250) {

                            //MEDIUM / FAST GESTURE: Pushing new data after 1800ms
                            // have passed @ speed more than 250 units.
                            long diff = System.currentTimeMillis() - timed;

                            if (diff > 2000) {
                                Log.d("timer", "Called after " + diff + "ms and time: " + count);

                                //LOG STATEMENT
                                Log.d("swipe", "The Swipe is FAST. X: " +
                                        Math.abs(mVelocityTracker.getXVelocity(pointerId)) + " | Y: " +
                                        Math.abs(mVelocityTracker.getYVelocity(pointerId)));

                                timed = System.currentTimeMillis();
                                ++count;
                                updateLedColor(getColor());
                            }

                        } else {
                            //SLOW GESTURE: Pushing new data after 1100ms
                            // have passed @ Speed less than 250 units.
                            long diff = System.currentTimeMillis() - timed;

                            if (diff > 1100) {
                                Log.d("timer", "Called after " + diff + "ms and time: " + count);

                                //LOG STATEMENT
                                Log.d("swipe", "The Swipe is SLOW. X: " +
                                        Math.abs(mVelocityTracker.getXVelocity(pointerId)) + " | Y: " +
                                        Math.abs(mVelocityTracker.getYVelocity(pointerId)));

                                timed = System.currentTimeMillis();
                                ++count;
                                updateLedColor(getColor());
                            }
                        }
                    }

                    if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                        mVelocityTracker.recycle();
                        mVelocityTracker = null;
                        timed = 0;
                    }

                };
        this.debounceHandler.postDelayed(debounceRunnable, this.debounceDuration);
        return true;
    }

    //updating the Server
    void updateLedColor(int color) {

        JSONObject desiredSettings = new JSONObject();
        try {

            String hex = String.format("#%06X", (0xFFFFFF & color));
            if (featureConfigColorTemp != null) {
                desiredSettings.put(featureConfigColorTemp.getName(), 0);
            }
            desiredSettings.put(featureConfigColor.getName(), Integer.valueOf(hex.substring(1), 16));
            Log.d("color", "color value is" + hex);

            parentActivity.updateSettings(desiredSettings);

        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
    }

    /**
     * gets a pixel color on the specific coordinate from the bitmap.
     *
     * @param x coordinate x.
     * @param y coordinate y.
     * @return selected color.
     */
    protected int getColorFromBitmap(float x, float y) {
        Matrix invertMatrix = new Matrix();
        palette.getImageMatrix().invert(invertMatrix);

        float[] mappedPoints = new float[]{x, y};
        invertMatrix.mapPoints(mappedPoints);

        if (palette.getDrawable() != null
                && palette.getDrawable() instanceof BitmapDrawable
                && mappedPoints[0] >= 0
                && mappedPoints[1] >= 0
                && mappedPoints[0] < palette.getDrawable().getIntrinsicWidth()
                && mappedPoints[1] < palette.getDrawable().getIntrinsicHeight()) {

            invalidate();

            Rect rect = palette.getDrawable().getBounds();
            float scaleX = mappedPoints[0] / rect.width();
            int x1 = (int) (scaleX * ((BitmapDrawable) palette.getDrawable()).getBitmap().getWidth());
            float scaleY = mappedPoints[1] / rect.height();
            int y1 = (int) (scaleY * ((BitmapDrawable) palette.getDrawable()).getBitmap().getHeight());
            return ((BitmapDrawable) palette.getDrawable()).getBitmap().getPixel(x1, y1);
        }
        return 0;
    }

    /**
     * sets a {@link ColorPickerViewListener} on the {@link ColorPickerView}.
     *
     * @param colorListener {@link ColorListener} or {@link ColorEnvelopeListener}.
     */
    public void setColorListener(ColorPickerViewListener colorListener) {
        this.colorListener = colorListener;
    }

    /**
     * invokes {@link ColorListener} or {@link ColorEnvelopeListener} with a color value.
     *
     * @param color    color.
     * @param fromUser triggered by user or not.
     */
    public void fireColorListener(int color, final boolean fromUser) {
        if (this.colorListener != null) {
            this.selectedColor = color;
            if (colorListener instanceof ColorListener) {
                ((ColorListener) colorListener).onColorSelected(selectedColor, fromUser);
            } else if (colorListener instanceof ColorEnvelopeListener) {
                ColorEnvelope envelope = new ColorEnvelope(selectedColor);
                ((ColorEnvelopeListener) colorListener).onColorSelected(envelope, fromUser);
            }

            if (VISIBLE_FLAG) {
                VISIBLE_FLAG = false;
                if (this.selector != null) {
                    this.selector.setAlpha(alpha_selector);
                }
            }
        }
    }

    /**
     * gets the selected color.
     *
     * @return the selected color.
     */
    public int getColor() {
        return selectedColor;
    }

    /**
     * sets the pure color.
     *
     * @param color the pure color.
     */
    public void setPureColor(int color) {
    }

    /**
     * gets the {@link ColorEnvelope} of the selected color.
     *
     * @return {@link ColorEnvelope}.
     */
    public ColorEnvelope getColorEnvelope() {
        return new ColorEnvelope(getColor());
    }

    /**
     * gets a debounce duration.
     *
     * <p>only emit a color to the listener if a particular timespan has passed without it emitting
     * another value.
     *
     * @return debounceDuration.
     */
    public long getDebounceDuration() {
        return this.debounceDuration;
    }

    /**
     * sets a debounce duration.
     *
     * <p>only emit a color to the listener if a particular timespan has passed without it emitting
     * another value.
     *
     * @param debounceDuration intervals.
     */
    public void setDebounceDuration(long debounceDuration) {
        this.debounceDuration = debounceDuration;
    }

    /**
     * gets center coordinate of the selector.
     *
     * @param x coordinate x.
     * @param y coordinate y.
     * @return the center coordinate of the selector.
     */
    private Point getCenterPoint(int x, int y) {
        return new Point(x - (selector.getMeasuredWidth() / 2), y - (selector.getMeasuredHeight() / 2));
    }

    /**
     * gets a selector's selected coordinate x.
     *
     * @return a selected coordinate x.
     */
    public float getSelectorX() {
        return selector.getX() - (selector.getMeasuredWidth() / 2);
    }

    /**
     * gets a selector's selected coordinate y.
     *
     * @return a selected coordinate y.
     */
    public float getSelectorY() {
        return selector.getY() - (selector.getMeasuredHeight() / 2);
    }

    /**
     * gets a selector's selected coordinate.
     *
     * @return a selected coordinate {@link Point}.
     */
    public Point getSelectedPoint() {
        return selectedPoint;
    }

    /**
     * changes selector's selected point with notifies about changes manually.
     *
     * @param x coordinate x of the selector.
     * @param y coordinate y of the selector.
     */
    public void setSelectorPoint(int x, int y) {
        Point mappedPoint = PointMapper.getColorPoint(this, new Point(x, y));
        int color = getColorFromBitmap(mappedPoint.x, mappedPoint.y);
        selectedColor = color;
        selectedPoint = new Point(mappedPoint.x, mappedPoint.y);
        setCoordinate(mappedPoint.x, mappedPoint.y);
        fireColorListener(getColor(), false);
    }

    /**
     * moves selector's selected point with notifies about changes manually.
     *
     * @param x coordinate x of the selector.
     * @param y coordinate y of the selector.
     */
    public void moveSelectorPoint(int x, int y, int color) {
        selectedColor = color;
        this.selectedPoint = new Point(x, y);
        setCoordinate(x, y);
        fireColorListener(getColor(), false);
    }

    /**
     * changes selector's selected point without notifies.
     *
     * @param x coordinate x of the selector.
     * @param y coordinate y of the selector.
     */
    public void setCoordinate(int x, int y) {
        selector.setX(x - (selector.getMeasuredWidth() / 2));
        selector.setY(y - (selector.getMeasuredHeight() / 2));
    }

    /**
     * changes selector's selected point by a specific color.
     *
     * <p>It may not work properly if change the default palette drawable.
     *
     * @param color color.
     */
    public void selectByHsv(int color) {
        int radius = getMeasuredWidth() / 2;

        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);

        double x = hsv[1] * Math.cos(Math.toRadians(hsv[0]));
        double y = hsv[1] * Math.sin(Math.toRadians(hsv[0]));

        int pointX = (int) ((x + 1) * radius);
        int pointY = (int) ((1 - y) * radius);

//        if (this.brightnessSlider != null) this.brightnessSlider.setSelectorPosition(hsv[2]);
        Point mappedPoint = PointMapper.getColorPoint(this, new Point(pointX, pointY));
        selectedColor = color;
        selectedPoint = new Point(mappedPoint.x, mappedPoint.y);
        setCoordinate(mappedPoint.x, mappedPoint.y);
        fireColorListener(getColor(), false);
//        notifyToFlagView(selectedPoint);
//        notifyToSlideBars();
    }

    /**
     * changes palette drawable manually.
     *
     * @param drawable palette drawable.
     */
    public void setPaletteDrawable(@NonNull Drawable drawable) {
        removeView(palette);
        palette = new ImageView(getContext());
        paletteDrawable = drawable;
        palette.setImageDrawable(paletteDrawable);
        addView(palette);

        removeView(selector);
        addView(selector);

        if (!VISIBLE_FLAG) {
            VISIBLE_FLAG = true;
            if (selector != null) {
                alpha_selector = selector.getAlpha();
                selector.setAlpha(0.0f);
            }
        }
    }

    /**
     * changes selector drawable manually.
     *
     * @param drawable selector drawable.
     */
    public void setSelectorDrawable(@NonNull Drawable drawable) {
        selector.setImageDrawable(drawable);
    }

    /**
     * selects the center of the palette manually.
     */
    public void selectCenter() {
        setSelectorPoint(getMeasuredWidth() / 2, getMeasuredHeight() / 2);
    }

    /**
     * gets an {@link ActionMode}.
     *
     * @return {@link ActionMode}.
     */
    public ActionMode getActionMode() {
        return this.actionMode;
    }

    /**
     * sets an {@link ActionMode}.
     *
     * @param actionMode {@link ActionMode}.
     */
    public void setActionMode(ActionMode actionMode) {
        this.actionMode = actionMode;
    }

    /**
     * linking an {@link BrightnessSlideBar} on the {@link ColorPickerView}.
     *
     * @param brightnessSlider {@link BrightnessSlideBar}.
     */

    /**
     * gets the preference name.
     *
     * @return preference name.
     */
    public String getPreferenceName() {
        return preferenceName;
    }

    /**
     * sets the preference name.
     *
     * @param preferenceName preference name.
     */
    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    /**
     * sets the {@link LifecycleOwner}.
     *
     * @param lifecycleOwner {@link LifecycleOwner}.
     */
    public void setLifecycleOwner(LifecycleOwner lifecycleOwner) {
        lifecycleOwner.getLifecycle().addObserver(this);
    }

    /**
     * removes this color picker observer from the the {@link LifecycleOwner}.
     *
     * @param lifecycleOwner {@link LifecycleOwner}.
     */
    public void removeLifecycleOwner(LifecycleOwner lifecycleOwner) {
        lifecycleOwner.getLifecycle().removeObserver(this);
    }

    /**
     * This method invoked by the {@link LifecycleOwner}'s life cycle.
     *
     * <p>OnDestroy would be called on the {@link LifecycleOwner}, all of the color picker data will
     * be saved automatically.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        ColorPickerPreferenceManager.getInstance(getContext()).saveColorPickerData(this);
    }

    /**
     * Builder class for create {@link ColorPickerView}.
     */
    public static class Builder {
        private final Context context;
        private final ActionMode actionMode = ActionMode.ALWAYS;
        private final float alpha_selector = 1.0f;
        private final float alpha_flag = 1.0f;
        private ColorPickerViewListener colorPickerViewListener;
        private int debounceDuration = 100;
        private Drawable paletteDrawable;
        private Drawable selectorDrawable;
        private int width = LayoutParams.MATCH_PARENT;
        private int height = LayoutParams.MATCH_PARENT;
        private String preferenceName;
        private LifecycleOwner lifecycleOwner;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setColorListener(ColorPickerViewListener colorPickerViewListener) {
            this.colorPickerViewListener = colorPickerViewListener;
            return this;
        }

        public Builder setDebounceDuration(int debounceDuration) {
            this.debounceDuration = debounceDuration;
            return this;
        }

        public Builder setPaletteDrawable(@NonNull Drawable palette) {
            this.paletteDrawable = palette;
            return this;
        }

        public Builder setWidth(int width) {
            this.width = width;
            return this;
        }

        public Builder setHeight(int height) {
            this.height = height;
            return this;
        }

        public Builder setPreferenceName(String preferenceName) {
            this.preferenceName = preferenceName;
            return this;
        }

        public Builder setLifecycleOwner(LifecycleOwner lifecycleOwner) {
            this.lifecycleOwner = lifecycleOwner;
            return this;
        }

        public ColorPickerView build() {
            ColorPickerView colorPickerView = new ColorPickerView(context);
            colorPickerView.onCreateByBuilder(this);
            return colorPickerView;
        }

        public void setSelectorDrawable(Drawable selectorDrawable) {
            this.selectorDrawable = selectorDrawable;
        }
    }
}


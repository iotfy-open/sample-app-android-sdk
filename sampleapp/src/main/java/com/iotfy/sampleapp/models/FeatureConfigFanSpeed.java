package com.iotfy.sampleapp.models;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FeatureConfigFanSpeed {

    public static final String FANSPEED_TYPE = "fanSpeed";
    public static final String LIST_TYPE = "list";
    String name;
    JSONObject config;

    public FeatureConfigFanSpeed(String name, JSONObject config) {
        this.name = name;
        this.config = config;
    }

    public String getType() throws JSONException {
        return config.getString("type");
    }

    public JSONObject getValuesJson() throws JSONException {
        return config.getJSONObject("data").getJSONObject("values");
    }

    public JSONArray getValuesJsonArray() throws JSONException {
        return config.getJSONObject("data").getJSONArray("values");
    }

    public String getLabel() throws JSONException {
        return config.getString("label");
    }

    public JSONObject getDataJson() throws JSONException {
        return config.optJSONObject("data");
    }

    public String getName() {
        return name;
    }

    public List<Integer> getValues() throws JSONException {

        List<Integer> valueList = new ArrayList<>();
        if (getType().equalsIgnoreCase(LIST_TYPE)) {
            JSONArray valuesJson = config.getJSONArray("values");
            for(int i=0; i<valuesJson.length();i++) {
                JSONObject valueJson = valuesJson.getJSONObject(i);
                valueList.add(valueJson.getInt("value"));
            }
            return valueList;
        } else {
            JSONObject valuesJson = config.getJSONObject("values");
            Iterator<String> valueIterator = valuesJson.keys();
            while (valueIterator.hasNext()) {
                String valueName = valueIterator.next();
                int value = valuesJson.getJSONObject(valueName).getInt("value");
                valueList.add(value);
            }
            return valueList;
        }
    }



    @NonNull
    @Override
    public String toString() {
        return "FeatureConfigFanSpeed{" +
                "name='" + name + '\'' +
                ", config=" + config.toString() +
                '}';
    }

}

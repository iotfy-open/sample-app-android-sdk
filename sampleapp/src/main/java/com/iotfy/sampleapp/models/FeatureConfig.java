package com.iotfy.sampleapp.models;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FeatureConfig {

    public static final String POWER_TYPE = "power";
    public static final String TOGGLE_TYPE = "toggle";
    public static final String LIST_TYPE = "list";
    public static final String MODE_TYPE = "mode";
    public static final String TIMER_TYPE = "timer";
    public static final String METRICS_TYPE = "metrics";
    public static final String TEMP_TYPE = "temperature";
    public static final String INPUT_TYPE = "input";
    public static final String DIMMER_TYPE = "dimmer";

    String name;
    JSONObject config;


    public FeatureConfig(String name, JSONObject config) {
        this.name = name;
        this.config = config;
    }

    public String getType() throws JSONException {
        return config.getString("type");
    }

    public JSONObject getValuesJson() throws JSONException {
        return config.getJSONObject("values");
    }

    public JSONObject getdataJson() throws JSONException {
        return config.optJSONObject("data");
    }

    public JSONObject getdataValuesJson() throws JSONException {
        return getdataJson().optJSONObject("values");
    }

    public JSONArray getValuesJsonArray() throws JSONException {
        return config.optJSONArray("values");
    }

    public String getLabel() throws JSONException {
        return config.getString("label");
    }

    public String getName() {
        return name;
    }

    public List<Integer> getValues() throws JSONException {

        List<Integer> valueList = new ArrayList<>();
        if (getType().equalsIgnoreCase(LIST_TYPE)) {
            JSONArray valuesJson = config.getJSONArray("values");
            for(int i=0; i<valuesJson.length();i++) {
                JSONObject valueJson = valuesJson.getJSONObject(i);
                valueList.add(valueJson.getInt("value"));
            }
            return valueList;
        } else {
            JSONObject valuesJson = config.getJSONObject("values");
            Iterator<String> valueIterator = valuesJson.keys();
            while (valueIterator.hasNext()) {
                String valueName = valueIterator.next();
                int value = valuesJson.getJSONObject(valueName).getInt("value");
                valueList.add(value);
            }
            return valueList;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "FeatureConfig{" +
                "name='" + name + '\'' +
                ", config=" + config.toString() +
                '}';
    }
}

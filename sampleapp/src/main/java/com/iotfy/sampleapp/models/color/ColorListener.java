package com.iotfy.sampleapp.models.color;

public interface ColorListener extends ColorPickerViewListener {
    /**
     * @param color    the last selected color.
     * @param fromUser triggered by the user(true) or not(false).
     */
    void onColorSelected(int color, boolean fromUser);

}

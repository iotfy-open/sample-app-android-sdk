package com.iotfy.sampleapp.models;

import org.json.JSONObject;

public class AdvancedFeature {

    public static final String NUMBER_TYPE = "field_number";
    public static final String TOGGLE_TYPE = "field_toggle";
    public static final String INPUT_TYPE = "input";

    private String featureName;
    private String inputType;
    private String label;
    private String level;
    private String valueType;
    private boolean isLevelA;
    private JSONObject dataJson;

    public AdvancedFeature(String featureName, JSONObject featureConfig) {
        this.featureName = featureName;
        if (featureConfig != null) {
            label = featureConfig.optString("label","");
            inputType = featureConfig.optString("type","");
            level = featureConfig.optString("level","");
            valueType = featureConfig.optString("ui","");
            dataJson = featureConfig.optJSONObject("data");

            isLevelA = level.equals("A");
        }
    }

    public String getFeatureName() {
        return featureName;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLevel() {
        return level;
    }

    public boolean isLevelA() {
        return isLevelA;
    }

    public String getValueType() {
        return valueType;
    }

    public JSONObject getDataJson() {
        return dataJson;
    }
}

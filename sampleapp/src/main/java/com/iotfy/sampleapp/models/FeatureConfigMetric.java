package com.iotfy.sampleapp.models;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FeatureConfigMetric {

    public static final String METRIC_TYPE = "metric";
    String name;
    JSONObject config;

    public FeatureConfigMetric(String name, JSONObject config) {
        this.name = name;
        this.config = config;
    }

    public String getType() throws JSONException {
        return config.getString("type");
    }

    public JSONObject getValuesJson() throws JSONException {
        return config.getJSONObject("data").getJSONObject("values");
    }

    public JSONArray getValuesJsonArray() throws JSONException {
        return config.getJSONObject("data").getJSONArray("values");
    }
    public JSONObject getDataJson() throws JSONException {
        return config.optJSONObject("data");
    }

    public String getLabel() throws JSONException {
        return config.getString("label");
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return "FeatureConfigMetric{" +
                "name='" + name + '\'' +
                ", config=" + config.toString() +
                '}';
    }

}

package com.iotfy.sampleapp.models;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FeatureConfigColor {
    public static final String COLOR_TYPE = "colorRGB";
    public static final String SECOND_COLOR_TYPE = "colorHSV";

    String name;
    JSONObject config;

    public FeatureConfigColor(String name, JSONObject config) {
        this.name = name;
        this.config = config;
    }

    public String getType() throws JSONException {
        return config.getString("type");
    }

    public JSONObject getValuesJson() throws JSONException {
        return config.getJSONObject("data").getJSONObject("values");
    }
    public JSONObject getLabesJson() throws JSONException {
        return config.getJSONObject("data").getJSONObject("labels");
    }

    public JSONArray getValuesJsonArray() throws JSONException {
        return config.getJSONObject("data").getJSONArray("values");
    }

    public String getLabel() throws JSONException {
        return config.getString("label");
    }

    public JSONObject getDataJson() throws JSONException {
        return config.optJSONObject("data");
    }

    public String getName() {
        return name;
    }


    @NonNull
    @Override
    public String toString() {
        return "FeatureConfigColor{" +
                "name='" + name + '\'' +
                ", config=" + config.toString() +
                '}';
    }

}

package com.iotfy.sampleapp.models;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FeatureConfigBrightness {
    public static final String BRIGHTNESS_TYPE = "brightness";

    String name;
    JSONObject config;

    public FeatureConfigBrightness(String name, JSONObject config) {
        this.name = name;
        this.config = config;
    }

    public String getType() throws JSONException {
        return config.getString("type");
    }

    public JSONObject getValuesJson() throws JSONException {
        return config.getJSONObject("data").getJSONObject("values");
    }
    public JSONObject getLabesJson() throws JSONException {
        return config.getJSONObject("data").getJSONObject("labels");
    }

    public JSONArray getValuesJsonArray() throws JSONException {
        return config.getJSONObject("data").getJSONArray("values");
    }

    public String getLabel() throws JSONException {
        return config.getString("label");
    }

    public JSONObject getDataJson() throws JSONException {
        return config.optJSONObject("data");
    }

    public String getName() {
        return name;
    }


    @NonNull
    @Override
    public String toString() {
        return "FeatureConfigBrighteness{" +
                "name='" + name + '\'' +
                ", config=" + config.toString() +
                '}';
    }

}

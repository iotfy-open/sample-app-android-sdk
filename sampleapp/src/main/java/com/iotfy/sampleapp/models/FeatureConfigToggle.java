package com.iotfy.sampleapp.models;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FeatureConfigToggle {
    public static final String TOGGLE_TYPE = "toggle";
    String name;
    JSONObject config;

    public FeatureConfigToggle(String name, JSONObject config) {
        this.name = name;
        this.config = config;
    }

    public String getType() throws JSONException {
        return config.getString("type");
    }

    public JSONObject getDataJson() throws JSONException {
        return config.getJSONObject("data");
    }

    public JSONArray getValuesJsonArray() throws JSONException {
        return config.getJSONArray("values");
    }

    public String getLabel() throws JSONException {
        return config.getString("label");
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return "FeatureConfigToggle{" +
                "name='" + name + '\'' +
                ", config=" + config.toString() +
                '}';
    }

}

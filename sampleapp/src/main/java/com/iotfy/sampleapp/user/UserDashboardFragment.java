package com.iotfy.sampleapp.user;

import androidx.fragment.app.Fragment;

import com.iotfy.magic.common.model.IoTfyDevice;

import org.json.JSONObject;

import java.util.List;

/**
 *
 */
public abstract class UserDashboardFragment extends Fragment {
    public abstract void updateUI(List<IoTfyDevice> smartThings);
    public abstract void updateUI(JSONObject roomsJson, String roomId);
    public abstract void deviceEnable(String udid);
    public abstract void deviceDisable(String udid);
}

package com.iotfy.sampleapp.user;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.sampleapp.NavigationController;
import com.iotfy.sampleapp.R;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UserThingsAdapter extends RecyclerView.Adapter<UserThingsAdapter.ThingViewHolder> {

    private final UserDashboardActivity activity;
    private final Map<String, ThingViewHolder> thingViewMap;
    private List<IoTfyDevice> userThings;

    public UserThingsAdapter(UserDashboardActivity activity, List<IoTfyDevice> things) {
        this.activity = activity;
        userThings = things;

        Iterator<IoTfyDevice> it = userThings.iterator();
        while (it.hasNext()) {
            IoTfyDevice thing = it.next();
            if (!IoTfyDevice.getSupportedDeviceTypes().contains(thing.getType())) {
                it.remove();
            }
        }
        thingViewMap = new HashMap<>();
    }


    @NonNull
    @Override
    public ThingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View thingView = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_user_things_card, parent, false);
        return new ThingViewHolder(thingView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ThingViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        final IoTfyDevice thing = userThings.get(position);
        thingViewMap.put(thing.getUdid(), holder);

        holder.thingNameTextView.setText(thing.getDeviceName());


        int iconResId = activity.getResources().getIdentifier(
                "ic_thing_dark_" + thing.getType(),
                "drawable",
                activity.getApplicationContext().getPackageName()
        );

        if (iconResId != 0) {
            holder.thingImageView.setImageDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), iconResId));
        } else {
            holder.thingImageView.setImageDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.ic_developer_board_black_48dp));
        }

        if (!thing.hasUserProvidedWifi()) {
            int disabledColorId = activity.getResources().getColor(R.color.cardBackgroundGray);
            holder.thingCardView.setCardBackgroundColor(disabledColorId);
            holder.thingPowerBtnRL.setVisibility(View.INVISIBLE);
            holder.thingCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Device is not connected to internet
                }
            });
        } else {
            holder.thingCardView.setCardBackgroundColor(Color.WHITE);
            holder.thingPowerBtnRL.setVisibility(View.VISIBLE);
            holder.thingCardView.setOnClickListener(v -> NavigationController.goToThingDashboard(activity, thing));
        }

        holder.quickOptionsAccessIv.setVisibility(View.GONE);
    }

    public void deviceEnable(String udid) {
        ThingViewHolder holder = thingViewMap.get(udid);
        if (holder == null) {
            return;
        }
        int enableColorId = activity.getResources().getColor(R.color.white);
        holder.thingCardView.setCardBackgroundColor(enableColorId);
        holder.thingPowerBtnRL.setVisibility(View.VISIBLE);
    }

    public void deviceDisable(String udid) {
        ThingViewHolder holder = thingViewMap.get(udid);

        if (holder == null) {
            return;
        }
        int disabledColorId = activity.getResources().getColor(R.color.cardBackgroundGray);
        holder.thingCardView.setCardBackgroundColor(disabledColorId);
        holder.thingPowerBtnRL.setVisibility(View.INVISIBLE);
    }


    public void removeAllThings(List<IoTfyDevice> things) {
        userThings.removeAll(things);
        notifyDataSetChanged();
    }

    public void removeThingFromView(String udid) {
        Iterator<IoTfyDevice> it = userThings.iterator();
        while (it.hasNext()) {
            IoTfyDevice thing = it.next();
            if (thing.getUdid().equalsIgnoreCase(udid)) {
                it.remove();
                notifyDataSetChanged();
                return;
            }
        }
    }

    @Override
    public int getItemCount() {
        return userThings.size();
    }


    static class ThingViewHolder extends RecyclerView.ViewHolder {
        CardView thingCardView;
        ImageView thingImageView;
        TextView thingNameTextView;
        TextView thingRoomNameTextView;
        SwitchCompat thingImagePower;
        RelativeLayout thingPowerBtnRL;
        ImageView quickOptionsAccessIv;

        ThingViewHolder(@NonNull View itemView) {
            super(itemView);
            thingCardView = itemView.findViewById(R.id.activtityUserThingsAdapterCv);
            thingImageView = itemView.findViewById(R.id.viewholder_user_things_cardadapter_imageView);
            thingNameTextView = itemView.findViewById(R.id.viewholder_user_things_card_textView_name);
            thingImagePower = itemView.findViewById(R.id.viewholder_user_things_card_power_imageView);
            thingPowerBtnRL = itemView.findViewById(R.id.viewholder_user_things_card_power_relativeLayout);
            thingRoomNameTextView = itemView.findViewById(R.id.viewholder_user_things_card_roomLabel_textView);
            quickOptionsAccessIv = itemView.findViewById(R.id.activityUserThingsAdapterMoreOption);
        }
    }
}

package com.iotfy.sampleapp.user;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.iotfy.magic.MagicSDK;
import com.iotfy.magic.common.callbacks.GetUserThingsCb;
import com.iotfy.magic.common.callbacks.LoginRequestCb;
import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.sampleapp.BuildConfig;
import com.iotfy.sampleapp.NavigationController;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.BaseActivity;

import java.lang.ref.WeakReference;
import java.util.List;

public class SplashActivity extends BaseActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private TextView errorTxt;
    private ProgressDialog progressDialog;

    private ProgressBar progressBar;
    private DataSyncHandler syncHandler;
    private boolean thingsRequestProcessed;
    private Button signUpBtn;
    private Button loginBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );

        initUI();

        syncHandler = new DataSyncHandler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (errorTxt != null) {
            errorTxt.setText("");
        }

        if (MagicSDK.getUserServiceInstance().isUserLoggedIn()) {
            getUserThings();
            if (syncHandler != null) {
                syncHandler.start();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (syncHandler != null) {
            syncHandler.stop();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initUI() {
        setContentView(R.layout.activity_splash);
        TextView appVersionTv = findViewById(R.id.activity_splash_app_version_textview);
        appVersionTv.setText(getString(R.string.splash_activity_app_version_tv, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));
        errorTxt = findViewById(R.id.activity_splash_error_msg_textview);
        progressBar = findViewById(R.id.activity_splash_progressbar);
        progressDialog = new ProgressDialog(this);
        signUpBtn = findViewById(R.id.activity_splash_signup_button);
        loginBtn = findViewById(R.id.activity_splash_login_button);

        if (MagicSDK.getUserServiceInstance().isUserLoggedIn()) {
            signUpBtn.setVisibility(View.GONE);
            loginBtn.setVisibility(View.GONE);
        } else {
            signUpBtn.setVisibility(View.VISIBLE);
            loginBtn.setVisibility(View.VISIBLE);
        }

        signUpBtn.setOnClickListener(v -> {
            signUpBtn.setText(getText(R.string.splash_activity_signing_txt));
            executeSignUpOrLoginRequest();
        });

        loginBtn.setOnClickListener(v -> {
            loginBtn.setText(getText(R.string.splash_activity_logining_txt));
            executeSignUpOrLoginRequest();
        });
    }

    public void executeSignUpOrLoginRequest() {
        loginBtn.setClickable(false);
        signUpBtn.setClickable(false);
        MagicSDK.getUserServiceInstance().loginOrRegisterSharedUser(
                "1075000105",
                new LoginRequestCb() {
                    @Override
                    public void onSuccess() {
                        finish();
                        startActivity(getIntent());
                    }

                    @Override
                    public void onError(String code, String error) {
                        loginBtn.setClickable(true);
                        signUpBtn.setClickable(true);
                        signUpBtn.setText(getText(R.string.activity_splash_sign_up_btn));
                        loginBtn.setText(getText(R.string.activity_splash_log_in_btn));
                    }
                });
    }

    private void updateErrorView(String msg) {
        errorTxt.setVisibility(View.VISIBLE);
        errorTxt.setText(msg);
        errorTxt.setTextColor(Color.BLACK);
    }

    private void getUserThings() {
        MagicSDK.getUserServiceInstance().getUserThings(
                new GetUserThingsCb() {
                    @Override
                    public void onSuccess(List<IoTfyDevice> devices) {
                        Log.d(TAG, devices.toString());
                    }

                    @Override
                    public void onError(String code, String error) {
                        Log.e(TAG, error);
                    }
                }
        );
        thingsRequestProcessed = true;
    }


    private static class DataSyncHandler extends Handler {
        private final WeakReference<SplashActivity> activityWeakRef;

        DataSyncHandler(SplashActivity activity) {
            activityWeakRef = new WeakReference<>(activity);
        }

        public void start() {
            sendEmptyMessageDelayed(0, 2000);
        }

        public void stop() {
            removeCallbacksAndMessages(null);
        }

        @Override
        public void handleMessage(Message msg) {
            SplashActivity activityInstance = activityWeakRef.get();
            if (activityInstance == null) {
                stop();
                return;
            }

            activityInstance.progressBar.setVisibility(View.VISIBLE);
            if (activityInstance.thingsRequestProcessed) {
                stop();
                NavigationController.goToUserDashboardFromSplash(activityInstance);
                activityInstance.finish();
            } else {
                sendEmptyMessageDelayed(0, 500);
            }
        }
    }
}

package com.iotfy.sampleapp.user;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.iotfy.magic.MagicSDK;
import com.iotfy.magic.common.callbacks.LogoutUserCb;
import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.sampleapp.NavigationController;
import com.iotfy.sampleapp.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserThingsFragment extends UserDashboardFragment {
    public static final String TAG = UserThingsFragment.class.getSimpleName();
    private UserDashboardActivity parentActivity;
    private UserThingsAdapter userThingsAdapter;
    private RecyclerView userThingsListingLayout;
    private CardView userDevicesCardView;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (UserDashboardActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_things_v2, container, false);
        progressBar = view.findViewById(R.id.thing_dash_pb);
        progressDialog = new ProgressDialog(parentActivity);
        FloatingActionButton addDevice = view.findViewById(R.id.user_thing_add_device);
        ImageView logoutIv = view.findViewById(R.id.fragment_user_rooms_addNewDevice_imageView);
        userDevicesCardView = view.findViewById(R.id.fragment_user_things_mydevices_cardView);
        progressBar.setVisibility(View.VISIBLE);
        logoutIv.setOnClickListener(v -> {
            progressDialog.setMessage("Logging out...");
            progressDialog.show();
            logoutUser();
        });

        addDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewDeviceIntent();
            }
        });


        userThingsListingLayout = view.findViewById(R.id.fragment_user_things_rv);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(parentActivity.getApplicationContext(), 2);
        userThingsListingLayout.setLayoutManager(layoutManager);
        userThingsListingLayout.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void addNewDeviceIntent() {
        NavigationController.goToAddDeviceActivity(parentActivity);
    }

    private void logoutUser() {
        MagicSDK.getUserServiceInstance().logoutUser(new LogoutUserCb() {
            @Override
            public void onSuccess() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                NavigationController.goToSplashActivity(parentActivity);
                Toast.makeText(parentActivity, getString(R.string.user_thing_logout_success_txt), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String code, String error) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(parentActivity, getString(R.string.user_thing_logout_failure_txt), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onStop() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        super.onStop();
    }

    @Override
    public void updateUI(List<IoTfyDevice> smartThings) {
        progressBar.setVisibility(View.INVISIBLE);
        if (smartThings.size() > 0) {
            userDevicesCardView.setVisibility(View.VISIBLE);
        } else {
            userDevicesCardView.setVisibility(View.GONE);
        }

        List<IoTfyDevice> userThings = parentActivity.getUserDevices();
        userThingsAdapter = new UserThingsAdapter(parentActivity, userThings);
        userThingsListingLayout.setAdapter(userThingsAdapter);

    }

    @Override
    public void updateUI(JSONObject roomsJson, String roomId) {

    }

    @Override
    public void deviceEnable(String udid) {
        userThingsAdapter.deviceEnable(udid);

    }

    @Override
    public void deviceDisable(String udid) {
        userThingsAdapter.deviceDisable(udid);
    }
}

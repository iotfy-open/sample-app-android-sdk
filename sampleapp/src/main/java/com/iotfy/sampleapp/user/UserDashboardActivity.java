package com.iotfy.sampleapp.user;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.iotfy.magic.MagicSDK;
import com.iotfy.magic.common.callbacks.GetUserThingsCb;
import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.BaseActivity;

import java.util.List;

public class UserDashboardActivity extends BaseActivity {

    public static final String TAG = UserDashboardActivity.class.getSimpleName();
    private UserDashboardFragment currentVisibleFragment;
    private ProgressDialog progressDialog;
    private List<IoTfyDevice> smartThings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_things);

        Window window = getWindow();
        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        window.setStatusBarColor(Color.TRANSPARENT);

        progressDialog = new ProgressDialog(this);

        loadFragment(new UserThingsFragment());
    }

    @Override
    protected void onStart() {
        super.onStart();
        MagicSDK.getUserServiceInstance().getUserThings(new GetUserThingsCb() {
            @Override
            public void onSuccess(List<IoTfyDevice> things) {
                smartThings = things;
                if (currentVisibleFragment != null) {
                    currentVisibleFragment.updateUI(things);
                }
            }

            @Override
            public void onError(String code, String error) {
                displayAlertDialog(code, "unable to load user things");
            }
        });
    }

    @Override
    protected void onStop() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void loadFragment(UserDashboardFragment dashboardFragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.activity_user_things_frameLayout, dashboardFragment);
        transaction.commit();
        currentVisibleFragment = dashboardFragment;
    }


    public List<IoTfyDevice> getUserDevices() {
        return smartThings;
    }
}

package com.iotfy.sampleapp.wifi;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.DhcpInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.iotfy.sampleapp.NavigationController;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.PermissionsActivity;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class WiFiSelectActivity extends PermissionsActivity {

    public static final String INTENT_REASON_ARG = "intent_reason";
    public static final String QUICK_CONNECT_REASON = "QUICK_CONNECT";
    public static final String WIFI_ADD_REASON = "WIFI_ADD_REASON";
    public static final String WIFI_UPDATE_REASON = "WIFI_UPDATE_REASON";

    public static final String UDID_ARG = "udid";

    private static final String TAG = WiFiSelectActivity.class.getSimpleName();
    private static final int MAX_WAIT_TIME_FOR_WIFI_CONNECTION_MS = 30000;

    private String udid;
    private WifiManager wifiManager;
    private String wifiname;
    private TextInputEditText wifiSsidInput;
    private TextInputEditText wifiPasswordInput;
    private TextInputLayout wifiPasswordTIL;

    private ImageView changeWifiIv;
    private ProgressBar progressBar;
    private Dialog dialog;

    private CountDownTimer wifiConnectionWaitTimer;
    private boolean isBackPressAllowed = true;
    private String intentReason;

    private byte[] convertAddressToBytes(int hostAddress) {
        return new byte[] { (byte)(0xff & hostAddress),
                (byte)(0xff & (hostAddress >> 8)),
                (byte)(0xff & (hostAddress >> 16)),
                (byte)(0xff & (hostAddress >> 24)) };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            Log.e(TAG, "Bundle is missing");
            displayToast(R.string.app_something_bad, Toast.LENGTH_LONG);
            finish();
            return;
        }

        intentReason = bundle.getString(INTENT_REASON_ARG);
        if (intentReason == null || intentReason.isEmpty()) {
            Log.e(TAG, "Intent reason is missing");
            displayToast(R.string.app_something_bad, Toast.LENGTH_LONG);
            finish();
            return;
        }

        switch (intentReason) {
            case WIFI_ADD_REASON:
            case WIFI_UPDATE_REASON:
                udid = bundle.getString(UDID_ARG);
                break;
            case QUICK_CONNECT_REASON:
                break;
            default:
                Log.e(TAG, "Invalid intent reason");
                displayToast(R.string.app_something_bad, Toast.LENGTH_LONG);
                finish();
                return;
        }

        // Init UI
        setContentView(R.layout.activity_wifi_select);
        wifiSsidInput = findViewById(R.id.activity_wifi_select_InputWifiSsid);
        changeWifiIv = findViewById(R.id.activity_wifi_select_changeWifi_imageView);
        progressBar = findViewById(R.id.activity_wifi_select_scanWifi_progressBar);
        wifiPasswordInput = findViewById(R.id.activity_wifi_select_inputWifiPassword_editText);
        wifiPasswordTIL = findViewById(R.id.activity_wifi_select_inputLayoutWifiPassword);

        ImageView backIv = findViewById(R.id.activity_wifi_select_back);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiConnectionWaitTimer = new CountDownTimer(MAX_WAIT_TIME_FOR_WIFI_CONNECTION_MS, 500) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (!wifiManager.isWifiEnabled()) {
                    return;
                }

                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
                    DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
                    if (dhcpInfo == null) {
                        return;
                    }
                    byte[] clientIpAddress = convertAddressToBytes(dhcpInfo.ipAddress);
                    String mobileIpAddr = "";
                    try {
                        mobileIpAddr = InetAddress.getByAddress(clientIpAddress).toString();
                    } catch (UnknownHostException ignored) {
                        return;
                    }

                    byte[] serverIpAddress = convertAddressToBytes(dhcpInfo.serverAddress);
                    String apIpAddr = "";
                    try {
                        apIpAddr = InetAddress.getByAddress(serverIpAddress).getHostAddress();
                        if (apIpAddr.length() == 0 || mobileIpAddr.length() == 0 || mobileIpAddr.equals("/0.0.0.0")) {
                            return;
                        }
                    } catch (UnknownHostException e) {
                        Log.e(TAG, e.getMessage());
                    }

                    // We have made sure that device is now connected with wifi
                    cancel();
                    onFinish();
                }
            }

            @Override
            public void onFinish() {
                changeWifiIv.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                wifiname = wifiManager.getConnectionInfo().getSSID();
                if (wifiname != null) {
                    wifiname = wifiname.replaceAll("^\"|\"$", "");
                    wifiSsidInput.setText(wifiname);
                    wifiPasswordInput.setText(null);
                }
            }
        };

        changeWifiIv.setOnClickListener(v -> {
            wifiSsidInput.setText(null);
            wifiname = "";
            NavigationController.goToPhoneWifiSettings(WiFiSelectActivity.this);
        });

        Button connectBtn = findViewById(R.id.activity_wifi_select_connect_button);
        connectBtn.setOnClickListener(v -> {
            isBackPressAllowed = false;
            if (wifiSsidInput.getText() == null) {
                wifiSsidInput.setError(getText(R.string.wfs_input_ssid_err));
                return;
            }

            String userWiFiSsid = wifiSsidInput.getText().toString().trim();
            if (userWiFiSsid.isEmpty()) {
                wifiSsidInput.requestFocus();
                wifiSsidInput.setError(getText(R.string.wfs_input_ssid_err));
                return;
            }

            if (wifiPasswordInput.getText() == null) {
                wifiPasswordTIL.setError(getText(R.string.wfs_input_password_err));
                return;
            }

            String userWiFiPassword = wifiPasswordInput.getText().toString().trim();
            if (userWiFiPassword.isEmpty()) {
                wifiPasswordInput.requestFocus();
                wifiPasswordTIL.setError(getText(R.string.wfs_input_password_err));
                return;
            }
            wifiPasswordTIL.setError(null);

            // Check for permissions
            if (appDoesNotHavePermission()) {
                return;
            }

            // Check if wifi is enabled
            if (!wifiManager.isWifiEnabled()) {
                displayWifiAlertDialog("Enabled Wifi", getString(R.string.app_err_wifi_disabled));
                return;
            }

            if (intentReason.equals(QUICK_CONNECT_REASON)) {
                NavigationController.quickConnectDevices(WiFiSelectActivity.this, userWiFiSsid, userWiFiPassword);
            }
            finish();
        });

        Button skipBtn = findViewById(R.id.activity_wifi_select_wifiDirect_button);
        TextView skipTv = findViewById(R.id.activity_wifi_select_skip_textView);
        TextView wiFiSelectTitle = findViewById(R.id.fragment_add_device_title);
        TextView wiFiSelectSubTitle = findViewById(R.id.activity_wifi_select_greeting_textView);
        ImageView quickConnectImage = findViewById(R.id.activity_wifi_select_wifi_imageView);
        TextView wiFiSelectHintText = findViewById(R.id.activity_wifi_select_hint_textView);
        LinearLayout quickSwitch = findViewById(R.id.activity_add_device_mode_switch_ll);
        quickSwitch.setVisibility(View.GONE);

        if (QUICK_CONNECT_REASON.equals(intentReason)) {
            wiFiSelectTitle.setText(getString(R.string.add_device));
            String hintText = "Select a 2.4 GHz Wi-Fi Network\n" + "and enter password";
            String wsTitle = "Quick Connect";
            quickConnectImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_connect_to_internet));
            wiFiSelectSubTitle.setText(wsTitle);
            wiFiSelectHintText.setText(hintText);
            skipBtn.setVisibility(View.INVISIBLE);
            skipTv.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check and ask if
        appDoesNotHavePermission();
    }

    @Override
    protected void onResume() {
        super.onResume();

        changeWifiIv.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        if (!wifiManager.isWifiEnabled()) {
            displayWifiAlertDialog("Enabled Wifi", getString(R.string.app_err_wifi_disabled));
            return;
        }

        if (wifiConnectionWaitTimer != null) {
            wifiConnectionWaitTimer.start();
        }

        wifiPasswordInput.setText(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (wifiConnectionWaitTimer != null) {
            wifiConnectionWaitTimer.cancel();
        }
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private boolean appDoesNotHavePermission() {
        if (!hasLocationAccess()) {
            requestLocationAccess();
            return true;
        } else if (!isLocationProviderEnabled()) {
            askToEnableLocationSharing();
            return true;
        }
        return false;
    }

    public void displayWifiAlertDialog(String title, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_custom_dialog);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button okbtn = dialog.findViewById(R.id.error_custom_dialog_ok_button);
        TextView titleTv = dialog.findViewById(R.id.error_custom_dialog_title);
        TextView msgTv = dialog.findViewById(R.id.error_custom_dialog_subtitle);
        titleTv.setText(title);
        msgTv.setText(message);
        dialog.setCancelable(false);
        okbtn.setOnClickListener(v -> {
            if (dialog != null && dialog.isShowing()) {
                NavigationController.goToPhoneWifiSettings(WiFiSelectActivity.this);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (intentReason != null && !intentReason.isEmpty()) {
            if (intentReason.equals(QUICK_CONNECT_REASON)) {
                NavigationController.goToAddDeviceActivity(this);
            } else if (intentReason.equals(WIFI_ADD_REASON)) {
                NavigationController.goToUserDashboard(this);
            } else
                finish();
        }
    }
}

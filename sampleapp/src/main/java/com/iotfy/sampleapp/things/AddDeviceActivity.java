package com.iotfy.sampleapp.things;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.sampleapp.NavigationController;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.PermissionsActivity;

public class AddDeviceActivity extends PermissionsActivity {

    private static final String QUICK_CONNECT_OPTION = "quick";

    private CardView deviceTypeCardView;
    private LinearLayout connectModeSwitchLayout;
    private TextView addDeviceModeText;
    private ImageView backImageView;
    private String connectOption;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);

        backImageView = findViewById(R.id.activity_add_device_back_iv);
        addDeviceModeText = findViewById(R.id.activity_add_device_mode_tv);
        connectModeSwitchLayout = findViewById(R.id.activity_add_device_mode_switch_ll);
        deviceTypeCardView = findViewById(R.id.activity_add_device_select_type_cv);
        // Default is quick connect mode
        connectOption = QUICK_CONNECT_OPTION;

        initUI();
    }

    private void initUI() {
        addDeviceModeText.setVisibility(View.VISIBLE);
        connectModeSwitchLayout.setVisibility(View.GONE);

        resetCardViews();

        RecyclerView deviceTypeRecycler = findViewById(R.id.activity_add_device_select_type_rv);
        deviceTypeRecycler.setLayoutManager(new LinearLayoutManager(this));
        SelectDeviceTypeAdapter selectDeviceTypeAdapter = new SelectDeviceTypeAdapter(
                this,
                IoTfyDevice.getSupportedDeviceTypes(),
                type -> {
                    if (connectOption.equalsIgnoreCase(QUICK_CONNECT_OPTION)) {
                        NavigationController.askWiFiForQuickConnect(AddDeviceActivity.this);
                        finish();
                    }
                }
        );

        backImageView.setOnClickListener(v -> resetLayoutOnBackPress());
        deviceTypeRecycler.setAdapter(selectDeviceTypeAdapter);
    }

    private void resetCardViews() {
        deviceTypeCardView.setVisibility(View.VISIBLE);
        addDeviceModeText.setVisibility(View.VISIBLE);
        connectModeSwitchLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        resetLayoutOnBackPress();
    }

    private void resetLayoutOnBackPress() {
        if (deviceTypeCardView.getVisibility() == View.VISIBLE) {
            NavigationController.goToUserDashboard(this);
        } else {
            resetCardViews();
        }
    }

}

package com.iotfy.sampleapp.things.plug;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.common.model.FeatureControl;
import com.iotfy.magic.common.model.FeatureMetric;
import com.iotfy.magic.common.model.StateReport;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.models.AdvancedFeature;
import com.iotfy.sampleapp.models.FeatureConfig;
import com.iotfy.sampleapp.models.FeatureConfigMetric;
import com.iotfy.sampleapp.models.FeatureConfigTimer;
import com.iotfy.sampleapp.things.DashboardFragment;
import com.iotfy.sampleapp.things.ThingDashboardActivity;
import com.iotfy.sampleapp.things.TimerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class PlugDashboardFragment extends DashboardFragment {

    public static final String TAG = PlugDashboardFragment.class.getSimpleName();
    private final List<AdvancedFeature> voltageConfigList = new ArrayList<>();
    private ThingDashboardActivity parentActivity;
    private ConstraintLayout feedbackLayout;
    private ImageButton powerBtn;
    private ImageView plugRepresenter;
    private TextView plugOffTv;
    private TimerAdapter timerAdapter;
    private VoltageAdapter voltageAdapter;
    private TextView voltageTextView;
    private TextView currentTextView;
    private TextView powerTextView;
    private TextView powerFactorTextView;
    private TextView voltageLabelTextView;
    private TextView currentLabelTextView;
    private TextView powerLabelTextView;
    private TextView faultIndicatorTextView;
    private TextView powerFactorLabelTextView;
    private boolean isDialogActive = false;
    private Dialog updateVoltageDialog;
    private FeatureConfig plugMetricsConfig;
    private FeatureConfigMetric featureConfigMetric;
    private FeatureConfigTimer timerFeatureConfig;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (ThingDashboardActivity) getActivity();

        JSONObject thingConfig = parentActivity.getConfig();
        if (thingConfig != null) {
            Iterator<String> featureIterator = thingConfig.keys();
            while (featureIterator.hasNext()) {
                String feature = featureIterator.next();
                try {
                    FeatureConfig featureConfig = new FeatureConfig(feature, thingConfig.getJSONObject(feature));
                    if (featureConfig.getType().equalsIgnoreCase(FeatureConfigTimer.TIMER_TYPE)) {
                        timerFeatureConfig = new FeatureConfigTimer(feature, thingConfig.getJSONObject(feature));
                    } else if (featureConfig.getType().equalsIgnoreCase(FeatureConfigMetric.METRIC_TYPE)) {
                        featureConfigMetric = new FeatureConfigMetric(feature, thingConfig.getJSONObject(feature));
                    } else if (featureConfig.getType().equalsIgnoreCase(AdvancedFeature.INPUT_TYPE)) {
                        voltageConfigList.add(new AdvancedFeature(feature, thingConfig.getJSONObject(feature)));
                    }

                } catch (JSONException jex) {
                    Log.w(TAG, jex.toString());
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plug_dashboard, container, false);

        LinearLayout plugDashPowerBtnLayout = view.findViewById(R.id.fragment_plug_dashboard_BtnPowerLayout);

        voltageTextView = view.findViewById(R.id.fragment_plug_dashboard_voltage_textView);
        currentTextView = view.findViewById(R.id.fragment_plug_dashboard_current_textView);
        powerTextView = view.findViewById(R.id.fragment_plug_dashboard_power_textView);
        powerFactorTextView = view.findViewById(R.id.fragment_plug_dashboard_power_factor_textview);
        voltageLabelTextView = view.findViewById(R.id.fragment_plug_dashboard_voltage_label_textView);
        currentLabelTextView = view.findViewById(R.id.fragment_plug_dashboard_current_label_textView);
        powerLabelTextView = view.findViewById(R.id.fragment_plug_dashboard_power_label_textView);
        powerFactorLabelTextView = view.findViewById(R.id.fragment_plug_dashboard_power_factor_label_textView);
        feedbackLayout = view.findViewById(R.id.fragment_plug_dashboard_feedbackLayout);
        powerBtn = view.findViewById(R.id.fragment_plug_dashboard_powerOnOff);
        plugOffTv = view.findViewById(R.id.fragment_plug_dashboard_powerOff_textView);
        plugRepresenter = view.findViewById(R.id.fragment_plug_dashboard_ImageView);
        faultIndicatorTextView = view.findViewById(R.id.fragment_plug_dashboard_fault_tv);

        RecyclerView voltageRecycler = view.findViewById(R.id.fragment_plug_dashboard_voltage_recycler);
        LinearLayout fragmentPlugVoltageInfo = view.findViewById(R.id.fragment_plug_dashboard_voltageInfo);
        voltageRecycler.setLayoutManager(new GridLayoutManager(view.getContext(), 2));

        if (voltageConfigList.size() > 0) {
            fragmentPlugVoltageInfo.setVisibility(View.VISIBLE);
            voltageAdapter = new VoltageAdapter(parentActivity, voltageConfigList);
            voltageRecycler.setAdapter(voltageAdapter);
        } else {
            fragmentPlugVoltageInfo.setVisibility(View.GONE);
        }

        LinearLayout timerLL = view.findViewById(R.id.fragment_powerStrip_dashboard_Timer_linearLayout);
        if (timerFeatureConfig == null) {
            timerLL.setVisibility(View.GONE);
        } else {
            RecyclerView plusTimerRv = view.findViewById(R.id.fragment_plug_dashboard_timer_recyclerView);
            plusTimerRv.setNestedScrollingEnabled(false);
            timerAdapter = new TimerAdapter(parentActivity, timerFeatureConfig, 0);
            plusTimerRv.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
            plusTimerRv.setAdapter(timerAdapter);
        }

        plugDashPowerBtnLayout.setOnClickListener(view1 -> {
            JSONObject desiredSettings = new JSONObject();
            try {
                if (parentActivity.getCurrentSettingsJson().optInt(FeatureControl.POWER, 0) == 0) {
                    desiredSettings.put(FeatureControl.POWER, 1);
                } else {
                    desiredSettings.put(FeatureControl.POWER, 0);
                }
                parentActivity.updateSettings(desiredSettings);
            } catch (JSONException jex) {
                Log.w(TAG, jex.toString());
            }
        });

        return view;
    }

    @Override
    public void onPause() {
        if (updateVoltageDialog != null && updateVoltageDialog.isShowing()) {
            updateVoltageDialog.dismiss();
        }

        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        updateConnectionState(parentActivity.isThingConnected(),
                parentActivity.getDisplayLabel());

        updateUI(parentActivity.getState());

    }

    private void enableControls() {
        if (timerFeatureConfig != null) {
            timerAdapter.enableUI();
        }
        if (!voltageConfigList.isEmpty()) {
            voltageAdapter.enableUI();
        }
    }

    private void disableControls() {
        if (timerFeatureConfig != null) {
            timerAdapter.disableUI();
        }
        if (!voltageConfigList.isEmpty()) {
            voltageAdapter.disableUI();
        }
        faultIndicatorTextView.setVisibility(View.GONE);
    }

    private void changeFeedbackTextViewVisibility(int visibility) {
        voltageTextView.setVisibility(visibility);
        currentTextView.setVisibility(visibility);
        powerTextView.setVisibility(visibility);
        powerFactorTextView.setVisibility(visibility);
        voltageLabelTextView.setVisibility(visibility);
        currentLabelTextView.setVisibility(visibility);
        powerLabelTextView.setVisibility(visibility);
        powerFactorLabelTextView.setVisibility(visibility);
    }

    private void resetUI() {
        if (timerFeatureConfig != null) {
            timerAdapter.resetTimersView();
        }
    }

    @Override
    public void updateUI(StateReport state) {
        if (parentActivity == null || state == null) {
            return;
        }

        JSONObject currentSettings;
        try {
            currentSettings = state.getSettings();
        } catch (NullPointerException ex) {
            Log.e(TAG, ex.toString());
            currentSettings = new JSONObject();
        }

        resetUI();

        // Power state
        int powerVal = 0;
        powerVal = currentSettings.optInt(FeatureControl.POWER, 1);

        Context fragContext = getContext();
        Resources appResources = null;
        if (fragContext != null) {
            appResources = fragContext.getResources();
        }

        if (appResources == null) {
            return;
        }

        if (powerVal == 0) {
            powerBtn.setColorFilter(appResources.getColor(R.color.powerOffColor));
            feedbackLayout.setBackgroundColor(appResources.getColor(R.color.feedbackViewOff));
            plugOffTv.setVisibility(View.VISIBLE);
            plugRepresenter.setVisibility(View.GONE);
            disableControls();
            changeFeedbackTextViewVisibility(View.GONE);
            return;
        } else {
            plugRepresenter.setVisibility(View.VISIBLE);
            powerBtn.setColorFilter(appResources.getColor(R.color.powerOnColor));
            plugOffTv.setVisibility(View.INVISIBLE);
            feedbackLayout.setBackground(appResources.getDrawable(R.drawable.feedback_view_background));
            enableControls();

            setFeedbackViewValues();
        }

        if (timerFeatureConfig != null) {
            timerAdapter.setSelectedTimer(currentSettings.optInt(timerFeatureConfig.getName()));
        }

        if (!voltageConfigList.isEmpty() && !isDialogActive) {
            for (AdvancedFeature advancedFeature : voltageConfigList) {
                voltageAdapter.setVoltageValue(advancedFeature.getFeatureName(), advancedFeature.getDataJson().optInt("default"));
            }
        }
    }

    @Override
    public void updateComponentName(String componentIdentifier, String componentName) {

    }

    public void setFeedbackViewValues() {

        JSONObject dataJson = null;
        JSONObject configJson = parentActivity.getConfig();
        try {
            dataJson = parentActivity.getState().getData();
        } catch (NullPointerException ex) {
            Log.e(TAG, ex.toString());
            dataJson = new JSONObject();
        }

        int faultIndicated = dataJson.optInt(FeatureMetric.FAULT, 0);
        switch (faultIndicated) {
            case 0:
                faultIndicatorTextView.setVisibility(View.GONE);
                changeFeedbackTextViewVisibility(View.GONE);
                if (configJson.has(FeatureMetric.VOLTAGE)) {
                    voltageLabelTextView.setVisibility(View.VISIBLE);
                    voltageTextView.setVisibility(View.VISIBLE);
                    Double voltage = dataJson.optDouble(FeatureMetric.VOLTAGE, 0.0);
                    voltageTextView.setText(String.format(Locale.ENGLISH, "%.0f", voltage) + " V");
                }

                if (configJson.has(FeatureMetric.CURRENT)) {
                    currentLabelTextView.setVisibility(View.VISIBLE);
                    currentTextView.setVisibility(View.VISIBLE);
                    Double current = dataJson.optDouble(FeatureMetric.CURRENT, 0.0);
                    currentTextView.setText(String.format(Locale.ENGLISH, "%.1f", current) + " A");
                }

                if (configJson.has(FeatureMetric.POWER)) {
                    powerLabelTextView.setVisibility(View.VISIBLE);
                    powerTextView.setVisibility(View.VISIBLE);
                    Double power = dataJson.optDouble(FeatureMetric.POWER, 0.0);
                    powerTextView.setText(String.format(Locale.ENGLISH, "%.1f", power) + " W");
                }

                if (configJson.has(FeatureMetric.POWER_FACTOR)) {
                    powerFactorLabelTextView.setVisibility(View.VISIBLE);
                    powerFactorTextView.setVisibility(View.VISIBLE);
                    Double powerFac = dataJson.optDouble(FeatureMetric.POWER_FACTOR, 0.0);
                    powerFactorTextView.setText(String.format(Locale.ENGLISH, "%.2f", powerFac));
                }
                break;
            case 1:
                changeFeedbackTextViewVisibility(View.GONE);
                faultIndicatorTextView.setVisibility(View.VISIBLE);
                faultIndicatorTextView.setText(R.string.fragment_plug_dashboard_low_voltage_cut_off);
                break;
            case 2:
                changeFeedbackTextViewVisibility(View.GONE);
                faultIndicatorTextView.setVisibility(View.VISIBLE);
                faultIndicatorTextView.setText(R.string.fragment_plug_dashboard_high_voltage_cut_off);
                break;
        }
    }

    public void voltageChangeDialog(Context context, int maxVal, int minVal, String featureName, String name, String defaultVal) {
        updateVoltageDialog = new Dialog(context);
        isDialogActive = true;
        //Setting Dialog attributes
        updateVoltageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = updateVoltageDialog.getWindow();
        if (window == null) {
            return;
        }

        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        updateVoltageDialog.setCancelable(false);
        updateVoltageDialog.setContentView(R.layout.dialog_switch_name);

        TextView dialogTitle = updateVoltageDialog.findViewById(R.id.dialog_switch_name_textView);
        dialogTitle.setText("Enter " + name + " value");
        Button updateBtn = updateVoltageDialog.findViewById(R.id.dialog_switch_name_close_button);
        Button cancelBtn = updateVoltageDialog.findViewById(R.id.dialog_switch_name_cancel_button);
        EditText editText = updateVoltageDialog.findViewById(R.id.dialog_switch_name_edittext);
        editText.setHint(context.getString(R.string.dialog_title_enter_value));
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        updateBtn.setText(parentActivity.getString(R.string.activity_thing_update_txt));
        editText.setText(defaultVal);
        updateBtn.setOnClickListener(v -> {
            if (editText.getText() != null) {
                String voltageValue = editText.getText().toString();
                int selVolt = Integer.parseInt(voltageValue);
                if (selVolt > maxVal || selVolt < minVal) {
                    editText.setError(" Range is between " + minVal + " to " + maxVal);
                } else {
                    editText.clearFocus();
                    updateBtn.setText(parentActivity.getString(R.string.fontawesome_updating));
                    JSONObject desiredSettings = new JSONObject();
                    try {
                        String value = editText.getText().toString();
                        desiredSettings.put(featureName, Integer.parseInt(value));
                        parentActivity.updateSettings(desiredSettings);
                        isDialogActive = false;
                        updateVoltageDialog.dismiss();

                    } catch (JSONException jex) {
                        Log.e(TAG, jex.toString());
                        isDialogActive = false;
                        updateVoltageDialog.dismiss();
                    }

                }
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDialogActive = false;
                updateVoltageDialog.dismiss();
            }
        });
        updateVoltageDialog.show();
    }

}

package com.iotfy.sampleapp.things.switches;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.common.model.ComponentStateReport;
import com.iotfy.magic.common.model.DeviceComponent;
import com.iotfy.magic.common.model.FeatureControl;
import com.iotfy.sampleapp.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SwitchBoardAdapter extends RecyclerView.Adapter<SwitchBoardAdapter.SwitchViewHolder> {

    public final String TAG = SwitchBoardAdapter.class.getSimpleName();

    private final Context context;
    private final boolean isSwitchNameEditable;

    private final List<DeviceComponent> switches;
    private final Map<String, ComponentStateReport> switchStateMap;
    private final Map<String, SwitchViewHolder> viewHolderMap;

    private OnSwitchInteractionListener onSwitchInteractionListener;

    SwitchBoardAdapter(@NonNull Context context, @NonNull List<DeviceComponent> switches, boolean isSwitchNameEditable) {
        this.context = context;
        this.switches = switches;
        this.isSwitchNameEditable = isSwitchNameEditable;
        viewHolderMap = new HashMap<>(switches.size());
        switchStateMap = new HashMap<>(switches.size());
    }

    @Override
    public void onViewAttachedToWindow(@NonNull SwitchViewHolder holder) {
        holder.setIsRecyclable(false);
        super.onViewAttachedToWindow(holder);
    }

    @NonNull
    @Override
    public SwitchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.viewholder_switches, parent, false);
        return new SwitchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SwitchViewHolder holder, int position) {
        final DeviceComponent aSwitch = switches.get(position);

        if (isSwitchNameEditable) {
            holder.editIv.setVisibility(View.VISIBLE);
        } else {
            holder.editIv.setVisibility(View.GONE);
        }

        holder.switchName.setText(aSwitch.getName());
        holder.editIv.setOnClickListener(v -> {
            if (onSwitchInteractionListener != null) {
                onSwitchInteractionListener.onEditNameClick(aSwitch);
            }
        });

        holder.switchOnOffFrame.setOnClickListener(v -> {
            if (holder.switchOnOffText.getText().toString().equalsIgnoreCase("on")){
                if (onSwitchInteractionListener != null) {
                    onSwitchInteractionListener.onToggleStateChange(aSwitch, false);
                }
            } else {
                if (onSwitchInteractionListener != null) {
                    onSwitchInteractionListener.onToggleStateChange(aSwitch, true);
                }
            }
        });

        holder.timerImg.setOnClickListener(v -> {
            if (onSwitchInteractionListener != null) {
                onSwitchInteractionListener.onTimerClick(aSwitch);
            }
        });

        ComponentStateReport componentState = switchStateMap.get(aSwitch.getId());
        if (componentState == null || componentState.getSettings().optInt(FeatureControl.POWER, 0) == 0) {
            updateSwitchUI(false, holder);
            holder.timerImg.setEnabled(false);
            holder.timerImg.setColorFilter(context.getResources().getColor(R.color.lightGray));
        } else {
            updateSwitchUI(true, holder);
            holder.timerImg.setEnabled(true);
            int timerValue = componentState.getSettings().optInt(FeatureControl.OFF_TIMER, 0);
            if (timerValue > 0) {
                holder.timerImg.setColorFilter(context.getResources().getColor(R.color.colorPrimary));
            } else {
                holder.timerImg.setColorFilter(context.getResources().getColor(R.color.lightGray));
            }
        }

        viewHolderMap.put(aSwitch.getId(), holder);
    }

    @Override
    public int getItemCount() {
        return switches.size();
    }

    private void updateSwitchUI(boolean isOn, SwitchViewHolder viewHolder){
        if (isOn){
            viewHolder.switchOnOffFrame.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            viewHolder.switchOnOffText.setText("ON");
        } else {
            viewHolder.switchOnOffFrame.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
            viewHolder.switchOnOffText.setText("OFF");
        }
    }

    public void disableUI() {
        for (SwitchViewHolder holder : viewHolderMap.values()) {
            holder.switchOnOffFrame.setEnabled(false);
            holder.switchOnOffFrame.setBackground(ContextCompat.getDrawable(context,R.drawable.rounded_corners));
            holder.timerImg.setEnabled(false);
            holder.timerImg.setColorFilter(context.getResources().getColor(R.color.lightGray));
        }
    }

    public void enableUI() {
        for (SwitchViewHolder holder : viewHolderMap.values()) {
            holder.timerImg.setEnabled(true);
            holder.switchOnOffFrame.setEnabled(true);
        }
    }

    public void updateSwitchName(String componentId, String newName) {
        for (int i=0; i<switches.size(); i++) {
            DeviceComponent aSwitch = switches.get(i);
            if (aSwitch.getId().equalsIgnoreCase(componentId)) {
                aSwitch.setName(newName);
                break;
            }
        }

        SwitchViewHolder holder = viewHolderMap.get(componentId);
        if (holder != null) {
            holder.switchName.setText(newName);
        }
    }

    public void updateSwitchState(@NonNull String componentId, @NonNull ComponentStateReport componentState) {
        switchStateMap.put(componentId, componentState);
        notifyDataSetChanged();
    }


    public void setOnSwitchInteractionListener(@NonNull OnSwitchInteractionListener onSwitchInteractionListener) {
        this.onSwitchInteractionListener = onSwitchInteractionListener;
    }

    public static class SwitchViewHolder extends RecyclerView.ViewHolder {

        TextView switchName;
        ImageView timerImg;
        ImageView editIv;
        FrameLayout switchOnOffFrame;
        TextView switchOnOffText;

        public SwitchViewHolder(@NonNull View itemView) {
            super(itemView);
            switchName = itemView.findViewById(R.id.viewholder_switches_schedule_name);
            timerImg = itemView.findViewById(R.id.viewholder_switches_schedule_timer_icon_imageView);
            editIv = itemView.findViewById(R.id.viewholder_switches_edit_icon);
            switchOnOffFrame = itemView.findViewById(R.id.viewholder_switches_device_action_onoff_frameLayout);
            switchOnOffText = itemView.findViewById(R.id.viewholder_switches_device_action_on_off_text);
        }

    }

    public interface OnSwitchInteractionListener {
        void onTimerClick(DeviceComponent switchComponent);

        void onEditNameClick(DeviceComponent switchComponent);

        void onToggleStateChange(DeviceComponent switchComponent, boolean isOn);
    }
}

package com.iotfy.sampleapp.things;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.sampleapp.R;

import java.util.List;

public class ConfiguredDevicesAdapter extends RecyclerView.Adapter<ConfiguredDevicesAdapter.ThingViewHolder> {

    private List<IoTfyDevice> deviceList;
    private Context context;

    ConfiguredDevicesAdapter(List<IoTfyDevice> deviceList, Context ctx) {
        this.deviceList = deviceList;
        this.context = ctx;
    }

    @NonNull
    @Override
    public ThingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_routines_add_a_device, parent, false);
        return new ConfiguredDevicesAdapter.ThingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ThingViewHolder holder, int position) {
        IoTfyDevice thing = deviceList.get(position);
        holder.deviceName.setText(thing.getDeviceName());

        int iconResId = context.getResources().getIdentifier(
                "ic_thing_dark_" + thing.getType(),
                "drawable",
                context.getApplicationContext().getPackageName()
        );

        if (iconResId != 0) {
            holder.deviceIcon.setImageDrawable(
                    ContextCompat.getDrawable(context.getApplicationContext(),iconResId));
        } else {
            holder.deviceIcon.setImageDrawable(
                    ContextCompat.getDrawable(context.getApplicationContext(), R.drawable.ic_developer_board_black_48dp));
        }
    }

    @Override
    public int getItemCount() {
        return deviceList.size();
    }

    static class ThingViewHolder extends RecyclerView.ViewHolder {
        ImageView deviceIcon;
        TextView deviceName;

        ThingViewHolder(@NonNull View itemView) {
            super(itemView);
            deviceIcon = itemView.findViewById(R.id.view_routines_add_a_device_deviceImage);
            deviceName = itemView.findViewById(R.id.view_routines_add_a_device_deviceName);
        }
    }

}

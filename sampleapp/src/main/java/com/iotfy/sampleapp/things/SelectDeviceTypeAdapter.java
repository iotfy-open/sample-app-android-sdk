package com.iotfy.sampleapp.things;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.common.model.DeviceType;
import com.iotfy.sampleapp.R;

import java.util.List;

public class SelectDeviceTypeAdapter extends RecyclerView.Adapter<SelectDeviceTypeAdapter.AddDeviceViewHolder> {

    private final List<Integer> devicesType;
    private final OnTypeSelection onTypeSelection;
    private final Context ctx;

    public SelectDeviceTypeAdapter(@NonNull Context ctx, @NonNull List<Integer> devicesType, @NonNull OnTypeSelection onTypeSelection) {
        this.ctx = ctx;
        this.devicesType = devicesType;
        this.onTypeSelection = onTypeSelection;
    }

    @NonNull
    @Override
    public AddDeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.viewholder_routines_add_a_device, parent, false);
        return new AddDeviceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddDeviceViewHolder holder, int position) {
        int deviceType = devicesType.get(position);

        holder.deviceName.setText(DeviceType.getDeviceTypeName(deviceType));
        int iconResId = ctx.getResources().getIdentifier(
                "ic_thing_dark_" + deviceType,
                "drawable",
                ctx.getPackageName()
        );

        if (iconResId != 0) {
            holder.deviceImage.setImageDrawable(ContextCompat.getDrawable(ctx, iconResId));
        } else {
            holder.deviceImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.ic_developer_board_black_48dp));
        }

        holder.itemView.setOnClickListener(v -> onTypeSelection.onTypeSelected(deviceType));
    }

    @Override
    public int getItemCount() {
        return devicesType.size();
    }

    public static class AddDeviceViewHolder extends RecyclerView.ViewHolder {

        ImageView deviceImage;
        TextView deviceName;

        public AddDeviceViewHolder(@NonNull View itemView) {
            super(itemView);
            deviceImage = itemView.findViewById(R.id.view_routines_add_a_device_deviceImage);
            deviceName = itemView.findViewById(R.id.view_routines_add_a_device_deviceName);
        }
    }

    interface OnTypeSelection {
        void onTypeSelected(int type);
    }
}

package com.iotfy.sampleapp.things.switches;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.MagicSDK;
import com.iotfy.magic.common.callbacks.UpdateThingNameCb;
import com.iotfy.magic.common.model.ComponentStateReport;
import com.iotfy.magic.common.model.DeviceComponent;
import com.iotfy.magic.common.model.DeviceType;
import com.iotfy.magic.common.model.FeatureControl;
import com.iotfy.magic.common.model.StateReport;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.things.DashboardFragment;
import com.iotfy.sampleapp.things.ThingDashboardActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class SwitchBoardFragment extends DashboardFragment {

    private ThingDashboardActivity parentActivity;
    private ImageButton switchBoardPowerBtn;
    private TextView switchBoardPowerOffTV;
    private ImageView switchImage;
    private RelativeLayout switchBoardfeedbackLayout;
    private Dialog dialog;
    private SwitchBoardAdapter switchBoardAdapter;
    private List<DeviceComponent> switches;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (ThingDashboardActivity) getActivity();
        if (parentActivity == null) {
            return;
        }

        switches = new ArrayList<>();
        for (DeviceComponent component: parentActivity.getThing().getComponentsByType(DeviceType.SINGLE_SWITCH)) {
            switches.add(component);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_switch_board, container, false);
        LinearLayout switchBoardDashPowerBtnLayout = view.findViewById(R.id.fragment_switchboard_dashboard_PowerLayout);

        switchBoardPowerBtn = view.findViewById(R.id.fragment_switchboard_dashboard_powerOnOff);
        switchBoardfeedbackLayout = view.findViewById(R.id.fragment_switchboard_dashboard_feedbackLayout);
        switchBoardPowerOffTV = view.findViewById(R.id.fragment_switchboard_dashboard_powerOff_textView);

        switchImage = view.findViewById(R.id.fragment_switchboard_dashboard_ImageView);
        LinearLayout switchesLayout = view.findViewById(R.id.fragment_switchboard_components_ll);

        int deviceIconId = R.drawable.ic_thing_dark_10;
        switchImage.setImageDrawable(ContextCompat.getDrawable(parentActivity, deviceIconId));

        // Main Power Button
        switchBoardDashPowerBtnLayout.setOnClickListener(powerBtn -> {
            JSONObject desiredSettings = new JSONObject();
            JSONObject currentSettings = parentActivity.getCurrentSettingsJson();
            int powerValue = currentSettings.optInt(FeatureControl.POWER, 0);

            try {
                if (powerValue == 0) {
                    desiredSettings.put(FeatureControl.POWER, 1);
                } else {
                    desiredSettings.put(FeatureControl.POWER, 0);
                }
                parentActivity.updateSettings(desiredSettings);
            } catch (JSONException jex) {
                Timber.w(jex.toString());
            }
        });

        // Switch Component UI
        if (switches.size() == 0) {
            switchesLayout.setVisibility(View.GONE);
        } else {
            switchesLayout.setVisibility(View.VISIBLE);

            RecyclerView switchesRv = view.findViewById(R.id.fragment_switchBoard_dashboard_switches_recycler);
            switchesRv.setLayoutManager(new GridLayoutManager(parentActivity, 2));

            switchBoardAdapter = new SwitchBoardAdapter(parentActivity, switches, true);
            switchBoardAdapter.setOnSwitchInteractionListener(new SwitchBoardAdapter.OnSwitchInteractionListener() {
                @Override
                public void onTimerClick(DeviceComponent switchComponent) {
                    showTimerDialog(switchComponent.getId(), switchComponent.getName());
                }

                @Override
                public void onEditNameClick(DeviceComponent switchComponent) {
                    showComponentRenameDialog(switchComponent.getId(), switchComponent.getName());
                }

                @Override
                public void onToggleStateChange(DeviceComponent switchComponent, boolean isOn) {
                    updateSwitchPower(switchComponent.getId(), isOn);
                }
            });

            switchesRv.setAdapter(switchBoardAdapter);
        }

        return view;
    }

    public void updateTimer(final String componentId, final String featureName, final int selectedTimer) {
        JSONObject currentSettings = parentActivity.getCurrentSettingsJson();
        JSONObject desiredSettings = new JSONObject();

        // Fetch current set timer value
        int setTimerValue = 0;
        if (componentId == null || componentId.isEmpty()) {
            setTimerValue = currentSettings.optInt(featureName, 0);
        } else {
            JSONObject componentStateJson = currentSettings.optJSONObject(componentId);
            try {
                setTimerValue = componentStateJson.optInt(featureName, 0);
            } catch (NullPointerException npe) {
                // DO NOTHING
            }
        }

        int desiredTimerValue = 0;
        if (setTimerValue != selectedTimer) {
            desiredTimerValue = selectedTimer;
        }

        try {
            if (componentId == null || componentId.isEmpty()) {
                desiredSettings.put(featureName, desiredTimerValue);
            } else {
                JSONObject componentDesiredJson = new JSONObject();
                componentDesiredJson.put(featureName, desiredTimerValue);
                desiredSettings.put(componentId, componentDesiredJson);
            }

            parentActivity.updateSettings(desiredSettings);

        } catch (JSONException jex) {
            Timber.e(jex.toString());
        }
    }

    private void updateSwitchPower(String componentId, boolean isOn) {
        JSONObject desiredSettings = new JSONObject();

        int desiredPowerState = 0;
        if (isOn) {
            desiredPowerState = 1;
        }

        try {
            JSONObject switchSettings = new JSONObject();
            switchSettings.put(FeatureControl.POWER, desiredPowerState);
            desiredSettings.put(componentId, switchSettings);
        } catch (JSONException jex) {
            Timber.e(jex.toString());
        }
        parentActivity.updateSettings(desiredSettings);
    }


    @Override
    public void onResume() {
        super.onResume();

        updateConnectionState(parentActivity.isThingConnected(),
                parentActivity.getDisplayLabel()
        );

        updateUI(parentActivity.getState());
    }

    @Override
    public void updateComponentName(String componentIdentifier, String componentName) {
        if (switchBoardAdapter != null) {
            updateSwitchName(componentIdentifier, componentName);
        }
    }

    public void resetUI() {
        switchImage.setVisibility(View.INVISIBLE);
    }

    public void disableUI() {
        if (switchBoardAdapter != null) {
            switchBoardAdapter.disableUI();
        }
    }

    public void enableUI() {
        if (switchBoardAdapter != null) {
            switchBoardAdapter.enableUI();
        }
    }

    public void showTimerDialog(String componentId, String label) {
        //Creating dialog
        dialog = new Dialog(parentActivity);
        //Setting Dialog attributes
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_switch_timer);
        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        //Finding views
        NumberPicker hourPicker = dialog.findViewById(R.id.dialog_switch_timer_number_picker_hour);
        NumberPicker minPicker = dialog.findViewById(R.id.dialog_switch_timer_number_picker_min);
        TextView componentName = dialog.findViewById(R.id.dialog_switch_timer_textView);

        RelativeLayout setTimerBtn = dialog.findViewById(R.id.dialog_switch_timer_set_relativeLayout);
        RelativeLayout resetTimerBtn = dialog.findViewById(R.id.dialog_switch_timer_reset_relativeLayout);

        componentName.setText(label);

        // TODO: Derive min and max from config
        hourPicker.setMaxValue(8);
        hourPicker.setMinValue(0);
        minPicker.setMaxValue(59);
        minPicker.setMinValue(0);

        // Check if timer is set currently
        JSONObject currentSettings = parentActivity.getCurrentSettingsJson();

        int timerValue = 0;
        JSONObject switchJson = currentSettings.optJSONObject(componentId);
        if (switchJson != null) {
            timerValue = switchJson.optInt(FeatureControl.OFF_TIMER, 0);
        }

        if (timerValue != 0) {
            int hours = timerValue / 3600;
            int minute = (timerValue % 3600) / 60;
            hourPicker.setValue(hours);
            minPicker.setValue(minute);
            resetTimerBtn.setVisibility(View.VISIBLE);
        } else {
            hourPicker.setValue(0);
            minPicker.setValue(0);
            resetTimerBtn.setVisibility(View.GONE);
        }

        //Setting [HOUR] timer change value listener
        hourPicker.setOnValueChangedListener((picker, oldVal, newVal) -> {
            if (newVal == 8) {
                minPicker.setValue(0);
            }
        });

        minPicker.setOnValueChangedListener((picker, oldVal, newVal) -> {
            if (hourPicker.getValue() == 8) {
                minPicker.setValue(0);
            }
        });

        setTimerBtn.setOnClickListener(v -> {
            int hour = hourPicker.getValue();
            int min = minPicker.getValue();
            hourPicker.clearFocus();
            minPicker.clearFocus();
            int timerValueInSec = (hour * 3600) + (min * 60);
            if (timerValueInSec == 0) {
                parentActivity.displayToast(parentActivity.getString(R.string.dialog_time_select_time_txt), Toast.LENGTH_SHORT);
                return;
            }

            JSONObject desiredSettings = new JSONObject();
            try {
                JSONObject switchSettings = new JSONObject();
                switchSettings.put(FeatureControl.OFF_TIMER, timerValueInSec);
                desiredSettings.put(componentId, switchSettings);
                parentActivity.updateSettings(desiredSettings);
            } catch (JSONException jex) {
                Timber.e(jex.toString());
            }

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        });

        resetTimerBtn.setOnClickListener(v -> {
            minPicker.setValue(0);
            hourPicker.setValue(0);
            JSONObject desiredSettings = new JSONObject();
            try {
                JSONObject switchSettings = new JSONObject();
                switchSettings.put(FeatureControl.OFF_TIMER, 0);
                desiredSettings.put(componentId, switchSettings);
                parentActivity.updateSettings(desiredSettings);
            } catch (JSONException jex) {
                Timber.e(jex.toString());
            }


            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void showComponentRenameDialog(String componentIdentifier, String label) {

        dialog = new Dialog(parentActivity);
        //Setting Dialog attributes
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update_component_name);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button updateBtn = dialog.findViewById(R.id.dialog_component_name_update_action_btn);
        Button cancelBtn = dialog.findViewById(R.id.dialog_component_name_cancel_action_btn);
        EditText componentNameEdt = dialog.findViewById(R.id.dialog_component_name_et);

        componentNameEdt.setText(label);
        updateBtn.setOnClickListener(v -> {
            if (componentNameEdt.getText() == null) {
                componentNameEdt.setError(parentActivity.getText(R.string.dilaog_name_change_warning_txt));
                return;
            }

            String name = componentNameEdt.getText().toString();
            if (name.isEmpty()) {
                componentNameEdt.setError(parentActivity.getText(R.string.dilaog_name_change_warning_txt));
                return;
            }

            if (name.length() < 2 || name.length() > 15) {
                componentNameEdt.requestFocus();
                componentNameEdt.setError("Please provide name between 3 to 15 characters");
                return;
            }

            componentNameEdt.clearFocus();
            updateBtn.setText("Updating...");
            updateBtn.setEnabled(false);
            cancelBtn.setEnabled(false);

            MagicSDK.getThingServiceInstance().updateName(
                    parentActivity.getThing().getUdid(),
                    componentIdentifier,
                    name,
                    new UpdateThingNameCb() {
                        @Override
                        public void onSuccess() {
                            updateSwitchName(componentIdentifier, name);
                            dialog.dismiss();
                        }

                        @Override
                        public void onError(String error) {
                            updateBtn.setText(parentActivity.getString(R.string.activity_thing_update_txt));
                            updateBtn.setEnabled(true);
                            cancelBtn.setEnabled(true);
                        }
                    }
            );
        });

        cancelBtn.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    public void updateSwitchName(String componentId, String newName) {
        for (DeviceComponent component: switches) {
            if (component.getId().equalsIgnoreCase(componentId)) {
                component.setName(newName);
                switchBoardAdapter.updateSwitchName(componentId, newName);
            }
        }
    }

    private void setFeedbackView(boolean showOn) {
        if (showOn) {
            switchBoardPowerOffTV.setVisibility(View.INVISIBLE);
            switchImage.setVisibility(View.VISIBLE);
            switchBoardfeedbackLayout.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.feedback_view_background, null));
        } else {
            switchImage.setVisibility(View.INVISIBLE);
            switchBoardPowerOffTV.setVisibility(View.VISIBLE);
            switchBoardfeedbackLayout.setBackgroundColor(getResources().getColor(R.color.feedbackViewOff));
        }
    }

    private void setMainPowerButtonState(boolean showOn) {
        if (showOn) {
            switchBoardPowerBtn.setColorFilter(getResources().getColor(R.color.powerOnColor));
        } else {
            switchBoardPowerBtn.setColorFilter(getResources().getColor(R.color.powerOffColor));
        }
    }

    private void updateComponentsUI(StateReport state) {
        // For Switches
        for (DeviceComponent component: switches) {
            ComponentStateReport componentState = state.getComponentState(component.getId());
            if (componentState != null) {
                switchBoardAdapter.updateSwitchState(component.getId(), componentState);
            }
        }
    }

    @Override
    public void updateUI(StateReport state) {
        if (parentActivity == null || state == null || parentActivity.isFinishing()) {
            return;
        }

        resetUI();

        // Power state
        int powerVal = state.getSettings().optInt(FeatureControl.POWER, 0);

        Context fragContext = getContext();
        Resources appResources = null;
        if (fragContext != null) {
            appResources = fragContext.getResources();
        }

        if (appResources == null) {
            return;
        }

        if (powerVal == 0) {
            setMainPowerButtonState(false);
            setFeedbackView(false);
            disableUI();
        } else {
            setMainPowerButtonState(true);
            setFeedbackView(true);
            enableUI();
            updateComponentsUI(state);
        }
    }
}

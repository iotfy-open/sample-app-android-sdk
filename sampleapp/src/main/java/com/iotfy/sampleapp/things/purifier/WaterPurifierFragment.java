package com.iotfy.sampleapp.things.purifier;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.iotfy.magic.common.model.StateReport;
import com.iotfy.sampleapp.base.ControlThingActivity;
import com.iotfy.sampleapp.things.DashboardFragment;
import com.iotfy.sampleapp.things.ThingDashboardActivity;

public class WaterPurifierFragment extends DashboardFragment {

    private static final String TAG = WaterPurifierFragment.class.getSimpleName();

    private ControlThingActivity parentActivity;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (ThingDashboardActivity) getActivity();
    }

    @Override
    public void updateUI(StateReport state) {
        if (parentActivity == null || state == null) {
            return;
        }

    }

    @Override
    public void updateComponentName(String componentIdentifier, String componentName) {

    }
}

package com.iotfy.sampleapp.things;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.MagicSDK;
import com.iotfy.magic.common.callbacks.QuickConnectTaskCb;
import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.sampleapp.NavigationController;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.PermissionsActivity;

import java.util.ArrayList;
import java.util.List;

public class QuickConnectActivity extends PermissionsActivity {

    public static final String TAG = QuickConnectActivity.class.getSimpleName();
    public static final String WIFI_SSID_ARG = "ssid";
    public static final String WIFI_PASSWORD_ARG = "password";

    private ProgressBar step2Pb;
    private ImageView step2Check;
    private TextView step2Txt;
    private Dialog dialog;
    private Drawable stepInactive;
    private Drawable stepCompleted;
    private String userWifiSsid;
    private String userWifiPassword;
    private WifiManager wifiManager;
    private int processedDevicesCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_connect);

        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            Log.e(TAG, "Bundle is missing");
            displayToast(R.string.app_something_bad, Toast.LENGTH_LONG);
            finish();
            return;
        }

        userWifiSsid = bundle.getString(WIFI_SSID_ARG);
        if (userWifiSsid == null || userWifiSsid.trim().isEmpty()) {
            Log.e(TAG, "Bundle is missing");
            displayToast(R.string.app_something_bad, Toast.LENGTH_LONG);
            finish();
            return;
        }

        userWifiPassword = bundle.getString(WIFI_PASSWORD_ARG);
        if (userWifiPassword == null || userWifiPassword.trim().isEmpty()) {
            Log.e(TAG, "Bundle is missing");
            displayToast(R.string.app_something_bad, Toast.LENGTH_LONG);
            finish();
            return;
        }

        stepInactive = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_process_check_disabled, null);
        stepCompleted = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_process_check_primary, null);


        step2Pb = findViewById(R.id.activity_qc_step_2_pb);
        step2Check = findViewById(R.id.activity_qc_step_2_check);
        step2Txt = findViewById(R.id.activity_qc_step_2_txt);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        markScanningStarted();
        MagicSDK.getThingServiceInstance().onboardViaQuickConnect(userWifiSsid, userWifiPassword, new QuickConnectTaskCb() {
            @Override
            public void onError(int errorCode) {
                runOnUiThread(() -> {
                    resetSteps();
                    switch (errorCode) {
                        case WIFI_SCAN_FAILED:
                        case WIFI_NOT_FOUND_NEARBY:
                        case NO_DEVICE_FOUND:
                            displayErrorDialog(R.string.app_error_dialog_title, R.string.app_wifi_scan_failed);
                            break;
                        case WIFI_DISABLED:
                            displayErrorDialog(R.string.app_error_dialog_title, R.string.app_err_wifi_disabled);
                            break;
                        case LOCATION_PERMISSION_MISSING:
                        case LOCATION_SHARING_DISABLED:
                            askToEnableLocationSharing();
                            break;
                        case WIFI_FREQUENCY_5GHZ:
                            displayErrorDialog(R.string.app_error_dialog_title, R.string.app_wifi_5GHz);
                            break;
                        case WIFI_SIGNAL_WEAK:
                            displayErrorDialog(R.string.app_error_dialog_title, R.string.app_wifi_scan_failed);
                            break;
                    }
                });
            }

            @Override
            public void onDeviceFound(String udid) {
                processedDevicesCount = processedDevicesCount + 1;
                if (processedDevicesCount > 0) {
                    step2Txt.setText(getString(R.string.activity_qc_scanning_txt, processedDevicesCount));
                } else {
                    step2Txt.setText(getString(R.string.activity_qc_scanning_txt, 0));
                }
            }

            @Override
            public void onCompleted(List<IoTfyDevice> things) {
                onConfigurationCompleted(things);

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void resetSteps() {
        step2Pb.setVisibility(View.GONE);
        step2Check.setVisibility(View.VISIBLE);
        step2Check.setImageDrawable(stepInactive);
    }


    private void markScanningStarted() {
        step2Check.setVisibility(View.GONE);
        step2Pb.setVisibility(View.VISIBLE);
    }


    private void onConfigurationCompleted(List<IoTfyDevice> configuredThings) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        step2Pb.setVisibility(View.GONE);
        step2Check.setVisibility(View.VISIBLE);
        step2Check.setImageDrawable(stepCompleted);

        if (processedDevicesCount > 0) {
            step2Txt.setText(getString(R.string.activity_qc_scanning_txt, processedDevicesCount));
        } else {
            step2Txt.setText(getString(R.string.activity_qc_scanning_txt, 0));
        }
        if (configuredThings.isEmpty()) {
            return;
        }
        NavigationController.goToUserDashboard(this);
    }


    private void displayConfiguredDevicesDialog(List<IoTfyDevice> things) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        ArrayList<String> udids = new ArrayList<>(things.size());
        for (IoTfyDevice thing : things) {
            udids.add(thing.getUdid());
            // IACEApp.getInstance().getTelemeter().logQuickConnectConfigurationSent(getUserId(), thing.getUdid(),attemptId);
        }

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_configured_devices);

        RecyclerView deviceRv = dialog.findViewById(R.id.dialog_configured_devices_rv);
        deviceRv.setLayoutManager(new LinearLayoutManager(dialog.getContext()));

        ConfiguredDevicesAdapter adapter = new ConfiguredDevicesAdapter(
                things,
                this
        );
        deviceRv.setAdapter(adapter);
        Button roomBtn = dialog.findViewById(R.id.dialog_configured_devices_add_to_room_btn);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
        }
        dialog.setCancelable(false);
        dialog.show();
    }

    private void displayErrorDialog(int titleResId, int contentResId) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_wifi_error);
        TextView dialogTitle = dialog.findViewById(R.id.dialog_wifi_error_title);
        TextView dialogSubtitle = dialog.findViewById(R.id.dialog_wifi_error_subtitle);
        Button okButton = dialog.findViewById(R.id.dialog_wifi_error_button);

        dialogTitle.setText(titleResId);
        dialogSubtitle.setText(contentResId);

        okButton.setOnClickListener(v -> {
            NavigationController.goToAddDeviceActivity(QuickConnectActivity.this);
            dialog.dismiss();
            finish();
        });

        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        }
        dialog.setCancelable(false);
        if (!isFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        // DO NOTHING
    }
}

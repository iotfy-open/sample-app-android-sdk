package com.iotfy.sampleapp.things.led;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.ControlThingActivity;
import com.iotfy.sampleapp.models.FeatureConfigColor;
import com.iotfy.sampleapp.models.FeatureConfigColorTemp;
import com.iotfy.sampleapp.models.color.ColorEnvelopeListener;
import com.iotfy.sampleapp.models.color.ColorPickerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LEDColorsAdapter extends RecyclerView.Adapter<LEDColorsAdapter.LEDColorsViewHolder> {

    private static final String TAG = LEDColorsAdapter.class.getSimpleName();

    private final List<Integer> displayListColor;
    private final List<Integer> valueColorList;
    private final ControlThingActivity parentActivity;
    private final Map<Integer, LEDColorsViewHolder> funcViewMap;
    private final FeatureConfigColorTemp featureConfigColorTemp;
    private final FeatureConfigColor featureConfigColor;
    private RecyclerView mRecyclerView;
    private Dialog dialog;

    public LEDColorsAdapter(ControlThingActivity parentActivity, List<Integer> displayListColor, List<Integer> valueColorList, FeatureConfigColor featureConfigColor,
                            FeatureConfigColorTemp featureConfigColorTemp) {
        this.parentActivity = parentActivity;
        this.displayListColor = displayListColor;
        this.valueColorList = valueColorList;
        this.funcViewMap = new HashMap<>();
        this.featureConfigColor = featureConfigColor;
        this.featureConfigColorTemp = featureConfigColorTemp;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }

    @NonNull
    @Override
    public LEDColorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_led_dashboard_rv, parent, false);
        return new LEDColorsViewHolder(view);
    }


    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull LEDColorsViewHolder holder, int position) {
        final int keyColor = displayListColor.get(position);
        final int valueColor = valueColorList.get(position);

        if (keyColor == parentActivity.getResources().getColor(R.color.black)) {
            holder.borderCV.setCardBackgroundColor(Color.WHITE);
            holder.colorCV.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_add_color));
        } else {
            holder.colorCV.setCardBackgroundColor(keyColor);
        }

        if (keyColor == parentActivity.getResources().getColor(R.color.white)) {
            holder.dayImage.setVisibility(View.VISIBLE);
            holder.colorCV.setVisibility(View.GONE);
            holder.dayImage.setOnClickListener(v -> updateWhiteGradient());
        } else {
            holder.dayImage.setVisibility(View.GONE);
            holder.colorCV.setVisibility(View.VISIBLE);
        }

        holder.colorCV.setOnClickListener(view -> {
            JSONObject desiredSettings = new JSONObject();
            String hexColor = String.format("#%06X", (0xFFFFFF & valueColor));
            Log.d(TAG, "hexform==" + hexColor);
            if (hexColor.equals("#000000")) {
                View dialogContentView = parentActivity.getLayoutInflater().inflate(
                        R.layout.dialog_led_dash_color,
                        mRecyclerView,
                        false
                );

                if (dialogContentView.getParent() != null) {
                    ((ViewGroup) dialogContentView.getParent()).removeView(dialogContentView);
                } else {
                    ColorPickerView.parentActivity = parentActivity;
                    ColorPickerView.featureConfigColor = featureConfigColor;
                    ColorPickerView.featureConfigColorTemp = featureConfigColorTemp;
                    displayColorPicker(null, 0);
                }

            } else {
                try {
                    if (featureConfigColorTemp != null) {
                        desiredSettings.put(featureConfigColorTemp.getName(), 0);
                    }
                    desiredSettings.put(featureConfigColor.getName(), Integer.valueOf(hexColor.substring(1), 16));
                    parentActivity.updateSettings(desiredSettings);
                } catch (JSONException jex) {
                    Log.d(TAG, jex.toString());
                }
            }
        });

        funcViewMap.put(keyColor, holder);

        // Set initial state
        JSONObject settingsJson = parentActivity.getCurrentSettingsJson();

        if (settingsJson.optInt("pow", 0) == 0) {
            disableUI();
        } else {
            enableUI();
        }

    }

    public void disableUI() {
        for (LEDColorsViewHolder holder : funcViewMap.values()) {
            holder.colorCV.setEnabled(false);
        }
    }

    public void enableUI() {
        for (LEDColorsViewHolder holder : funcViewMap.values()) {
            holder.colorCV.setEnabled(true);
        }
    }

    public void displayColorPicker(Bitmap bitmapIv, int requestCode) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = new Dialog(parentActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_colorpicker);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }
        final int CAMERA_REQUEST = 1;
        final int GALLERY_PICTURE = 2;

        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        RelativeLayout colorFromImageRl = dialog.findViewById(R.id.layout_select_color_from_image);
        FrameLayout colorFromWheelFL = dialog.findViewById(R.id.colorPickerViewFrame);
        ImageView selectionModeIv = dialog.findViewById(R.id.layout_dialog_select_option_iv);
        Button okbtn = dialog.findViewById(R.id.layout_dialog_colorpicker_ok_btb);
        ColorPickerView colorPickerView = dialog.findViewById(R.id.ColorPickerView);
        RelativeLayout selectFromCameraRL = dialog.findViewById(R.id.layout_dialog_color_camera_RL);
        RelativeLayout selectFromGalleryRL = dialog.findViewById(R.id.layout_dialog_color_file_RL);

        selectFromCameraRL.setOnClickListener(v -> fireCameraIntent(CAMERA_REQUEST));

        selectFromGalleryRL.setOnClickListener(v -> {
            if (parentActivity.checkExternalStoragePermission()) {
                Intent pictureActionIntent = null;

                pictureActionIntent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                parentActivity.startActivityForResult(
                        pictureActionIntent,
                        GALLERY_PICTURE);
            }

        });

        selectionModeIv.setOnClickListener(v -> {
            if (colorFromImageRl.isShown()) {
                colorFromImageRl.setVisibility(View.GONE);
                colorFromWheelFL.setVisibility(View.VISIBLE);
                selectionModeIv.setImageDrawable(parentActivity.getResources().getDrawable(R.drawable.ic_led_image_icon));
            } else {
                colorFromImageRl.setVisibility(View.VISIBLE);
                colorFromWheelFL.setVisibility(View.GONE);
                selectionModeIv.setImageDrawable(parentActivity.getResources().getDrawable(R.drawable.ic_led_color_pallette));
            }

        });

        if (requestCode != 0) {
            colorFromImageRl.setVisibility(View.GONE);
            colorFromWheelFL.setVisibility(View.VISIBLE);
            if (bitmapIv != null) {
                Drawable colorImage = new BitmapDrawable(parentActivity.getResources(), bitmapIv);
                colorPickerView.setPaletteDrawable(colorImage);
            }

        }


        colorPickerView.setColorListener(
                (ColorEnvelopeListener) (envelope, fromUser) -> {
                    // nothing

                });

        okbtn.setOnClickListener(v -> {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void fireCameraIntent(int requestCode) {
        if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            parentActivity.startActivityForResult(intent, requestCode);

        } else {
            ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);

        }
    }


    @Override
    public int getItemCount() {
        return valueColorList.size();
    }

    public void updateWhiteGradient() {
        JSONObject desiredSettings = new JSONObject();
        try {

            if (featureConfigColorTemp != null) {
                JSONObject colorTemp = featureConfigColorTemp.getValuesJson();
                int colorTemperature = colorTemp.optInt("max", 0);
                desiredSettings.put(featureConfigColorTemp.getName(), colorTemperature);
            }

            if (featureConfigColor != null) {
                desiredSettings.put(featureConfigColor.getName(), 0);
            }

            parentActivity.updateSettings(desiredSettings);
        } catch (JSONException jex) {
            Log.e(TAG, jex.toString());
        }
    }

    static class LEDColorsViewHolder extends RecyclerView.ViewHolder {
        private final CardView borderCV;
        private final CardView colorCV;
        private final ImageView dayImage;

        LEDColorsViewHolder(@NonNull View itemView) {
            super(itemView);
            borderCV = itemView.findViewById(R.id.fragment_led_color_holder_cv);
            colorCV = itemView.findViewById(R.id.fragment_led_dashboard_recyclerView_LedDashPalette);
            dayImage = itemView.findViewById(R.id.day_image);
        }
    }
}

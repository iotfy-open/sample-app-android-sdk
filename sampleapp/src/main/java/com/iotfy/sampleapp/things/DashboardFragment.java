package com.iotfy.sampleapp.things;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.magic.common.model.StateReport;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.ControlThingActivity;
import com.iotfy.sampleapp.things.led.LEDDashboardFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class DashboardFragment extends Fragment {

    private static final String TAG = DashboardFragment.class.getSimpleName();
    private View offlineFilterView;
    private TextView wifiDirectTextView;

    private Dialog dialog;

    public abstract void updateUI(StateReport state);

    public abstract void updateComponentName(String componentIdentifier, String componentName);

    public void updateConnectionState(boolean connected, String deviceName) {
        if (offlineFilterView == null) {
            Log.d(TAG, "view is null");
            return;
        }

        if (getActivity() == null || getContext() == null) {
            return;
        }

        // remove view if connected
        if (connected) {
            offlineFilterView.setVisibility(View.GONE);
        } else {
            TextView textView = offlineFilterView.findViewById(R.id.window_offline_repeat_textView);
            String label = getString(R.string.not_connected, deviceName);
            textView.setText(label);

            //disable onTouch
            offlineFilterView.setOnTouchListener((v, event) -> {
                // ignore all touch events
                return true;
            });

            offlineFilterView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        offlineFilterView = view.findViewById(R.id.dashFragmentOfflineViewFilter);
        View fragmentActionbarView = view.findViewById(R.id.fragment_dashboard_actionbarLayout);
        if (fragmentActionbarView != null) {
            initActionBar(fragmentActionbarView);
        }
    }

    @Override
    public void onDestroyView() {
        if (dialog != null) {
            dialog.dismiss();
        }
        super.onDestroyView();
    }

    private void initActionBar(View actionBarRootView) {
        ImageView goback = actionBarRootView.findViewById(R.id.fragment_dash_actionBar_relativeLayout);
        LinearLayout wifiActionLayout = actionBarRootView.findViewById(R.id.fragment_dash_actionBar_view_wifiAction_layout);
        wifiDirectTextView = actionBarRootView.findViewById(R.id.fragment_dash_actionbar_textView);
        TextView devicename = actionBarRootView.findViewById(R.id.fragment_dash_actionBar_view_deviceName);
        TextView roomName = actionBarRootView.findViewById(R.id.fragment_dash_actionBar_view_Label_textView);
        ImageView connectedToInternetIv = actionBarRootView.findViewById(R.id.fragment_dash_actionBar_view_internetMode);
        ImageView directConnectIv = actionBarRootView.findViewById(R.id.fragment_dash_actionBar_view_wifiDirectMode);
        ControlThingActivity activity = (ControlThingActivity) getActivity();

        if (activity != null) {
            if (activity instanceof ThingDashboardActivity) {
                IoTfyDevice thing = ((ThingDashboardActivity) activity).getThing();
                if (activity.getDashboardFragmentClass(thing.getType()) == LEDDashboardFragment.class) {
                    roomName.setTextColor(Color.BLACK);
                    devicename.setTextColor(Color.BLACK);
                    goback.setColorFilter(Color.BLACK);
                    connectedToInternetIv.setColorFilter(Color.BLACK);
                    wifiDirectTextView.setTextColor(Color.BLACK);
                    wifiActionLayout.setBackground(activity.getResources().getDrawable(R.drawable.rounded_black_outline));
                    directConnectIv.setColorFilter(Color.BLACK);
                }


                if (devicename != null) {
                    devicename.setText(thing.getDeviceName());
                }
            }

            if (goback != null) {
                goback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ControlThingActivity activity = (ControlThingActivity) getActivity();
                        if (activity != null) {
                            activity.onBackPressed();
                        }
                    }
                });
            }
        }
    }

    private boolean appDoesNotHavePermission(ControlThingActivity activity) {
        if (!activity.hasLocationAccess()) {
            activity.requestLocationAccess();
            wifiDirectTextView.setText(getString(R.string.wifi_direct_text));
            return true;
        } else if (!activity.isLocationProviderEnabled()) {
            activity.askToEnableLocationSharing();
            wifiDirectTextView.setText(getString(R.string.wifi_direct_text));
            return true;
        }
        return false;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void showThingNotConnectedToInternetDialog() {
        ThingDashboardActivity activity = (ThingDashboardActivity) getActivity();

        if (activity != null) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            Window window = dialog.getWindow();
            if (window == null) {
                return;
            }
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_connect_to_internet);

            TextView notConnected = dialog.findViewById(R.id.dialog_connect_to_internet_not_connected_to_internet_textview);
            notConnected.setText(activity.getString(R.string.dialog_title_not_connected_to_internet, activity.getThing().getDeviceName()));
            Button connectBtn = dialog.findViewById(R.id.dialog_connect_to_internet_connect_button);
            Button cancelBtn = dialog.findViewById(R.id.dialog_connect_to_internet_cancel_button);

            connectBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!activity.getWifiManager().isWifiEnabled()) {
                        dialog.dismiss();
                        activity.displayWifiAlertDialog("Enable WiFi", activity.getString(R.string.app_err_wifi_disabled));
                        return;
                    }
                }
            });

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }

    }
}

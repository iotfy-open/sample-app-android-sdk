package com.iotfy.sampleapp.things;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.ControlThingActivity;
import com.iotfy.sampleapp.models.FeatureConfigTimer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TimerAdapter extends RecyclerView.Adapter<TimerAdapter.TimerBtnViewHolder> {
    private static final String TAG = TimerAdapter.class.getSimpleName();

    private ControlThingActivity parentActivity;
    private Map<Integer, TimerBtnViewHolder> viewHolderMap;
    private List<Map<String, String>> availableTimers;
    private Drawable stateIndicatorDrawableOn;
    private Drawable stateIndicatorDrawableOff;
    private FeatureConfigTimer featureConfigTimer;
    private int timeinSecs;

    public TimerAdapter(ControlThingActivity parentActivity, FeatureConfigTimer availableTimer, int timerSecs) {
        this.timeinSecs = timerSecs;
        this.featureConfigTimer = availableTimer;
        this.parentActivity = parentActivity;
        this.viewHolderMap = new HashMap<>();
        availableTimers = new ArrayList<>();
        try {
            JSONObject valuesJson = availableTimer.getdataJson().getJSONObject("labels");
            Iterator<String> modesIterator = valuesJson.keys();
            while (modesIterator.hasNext()) {
                String name = modesIterator.next();

                Map<String, String> timer = new HashMap<>();
                timer.put("name", valuesJson.getString(name));
                timer.put("value", name);
                availableTimers.add(timer);

            }
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
        Collections.sort(availableTimers, mapComparator);

    }

    public TimerAdapter(ControlThingActivity parentActivity, List<Map<String, String>> availableTimers, int timerSecs) {
        this.timeinSecs = timerSecs;
        this.parentActivity = parentActivity;
        this.viewHolderMap = new HashMap<>();
        this.availableTimers = availableTimers;
    }

    @NonNull
    @Override
    public TimerBtnViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_thing_dash_toggle_functions_rv, parent, false);
        return new TimerBtnViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TimerBtnViewHolder holder, final int position) {
        String timerName = "";
        int timeinSecs = 0;
        Map<String, String> map = availableTimers.get(position);

        timerName = map.get("name");
        timeinSecs = Integer.parseInt(Objects.requireNonNull(map.get("value")));

        holder.functionBtn.setText(timerName);
        final int selectedTimer = timeinSecs;

        holder.functionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject currentSettings = parentActivity.getCurrentSettingsJson();
                JSONObject desiredSettings = new JSONObject();
                try {
                    if (currentSettings.optInt(featureConfigTimer.getName(), 0) == selectedTimer) {
                        desiredSettings.put(featureConfigTimer.getName(), 0);
                    } else {
                        desiredSettings.put(featureConfigTimer.getName(), selectedTimer);
                    }

                    parentActivity.updateSettings(desiredSettings);
                } catch (JSONException jex) {
                    Log.e(TAG, jex.toString());
                }
            }
        });

        stateIndicatorDrawableOn = ResourcesCompat.getDrawable(parentActivity.getResources(),
                R.drawable.ic_state_button_indicator_on, null);
        stateIndicatorDrawableOff =
                ResourcesCompat.getDrawable(parentActivity.getResources(), R.drawable.ic_state_button_indicator_off,
                        null);
        viewHolderMap.put(timeinSecs, holder);

        JSONObject currentSettings = parentActivity.getCurrentSettingsJson();
        if (currentSettings == null || currentSettings.optInt("pow", 0) == 0) {
            resetTimersView();
            disableUI();
        } else {
            int timer = currentSettings.optInt(featureConfigTimer.getName(), 0);
            resetTimersView();
            setSelectedTimer(timer);
        }
    }

    public void resetTimersView() {
        for (TimerBtnViewHolder holder : viewHolderMap.values()) {
            holder.functionBtn.setCompoundDrawablesWithIntrinsicBounds(stateIndicatorDrawableOff, null, null, null);
        }
    }

    public void disableUI() {
        for (TimerBtnViewHolder holder : viewHolderMap.values()) {
            holder.functionBtn.setEnabled(false);
        }
    }

    public void enableUI() {
        for (TimerBtnViewHolder holder : viewHolderMap.values()) {
            holder.functionBtn.setEnabled(true);
        }
    }

    public void setSelectedTimer(int timerValue) {
        TimerBtnViewHolder holder = viewHolderMap.get(timerValue);
        if (holder != null) {
            holder.functionBtn.setCompoundDrawablesWithIntrinsicBounds(stateIndicatorDrawableOn, null, null, null);
        }
    }

    @Override
    public int getItemCount() {
        return availableTimers.size();
    }

    public Comparator<Map<String, String>> mapComparator = new Comparator<Map<String, String>>() {
        public int compare(Map<String, String> m1, Map<String, String> m2) {
            if (Integer.parseInt(m1.get("value")) > (Integer.parseInt(m2.get("value")))) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    public static class TimerBtnViewHolder extends RecyclerView.ViewHolder {
        private Button functionBtn;

        public TimerBtnViewHolder(@NonNull View itemView) {
            super(itemView);
            functionBtn = itemView.findViewById(R.id.fragment_switchBoard_dashboard_thingToggle_button);
        }
    }
}

package com.iotfy.sampleapp.things;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.iotfy.magic.MagicSDK;
import com.iotfy.magic.common.callbacks.ThingStateChangeCb;
import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.magic.common.model.IoTfyDeviceState;
import com.iotfy.magic.common.model.StateReport;
import com.iotfy.sampleapp.NavigationController;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.ControlThingActivity;
import com.iotfy.sampleapp.things.led.LEDDashboardFragment;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import timber.log.Timber;

public class ThingDashboardActivity extends ControlThingActivity {

    private static final String TAG = ThingDashboardActivity.class.getSimpleName();
    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_PICTURE = 2;

    private String udid;
    private IoTfyDevice thing;
    private StateReport state;
    private long lastUpdateTs;

    private DashboardFragment currentVisibleFragment;
    private Dialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thing_dashboard);
        Bundle b = getIntent().getExtras();
        if (b == null) {
            Log.e(TAG, "Bundle not found");
            finish();
            onBackPressed();
            return;
        }

        thing = (IoTfyDevice) b.getSerializable("thing");

        if (thing == null) {
            Log.e(TAG, "Data corruption");
            finish();
            onBackPressed();
            return;
        }

        udid = thing.getUdid();
        loadThingDashboard();
    }

    @Override
    protected void onStart() {
        super.onStart();
        lastUpdateTs = 0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_PICTURE)
                onSelectFromGalleryResult(data, requestCode);
            else if (requestCode == CAMERA_REQUEST)
                onCaptureImageResult(data, requestCode);
        }
    }

    private void onSelectFromGalleryResult(Intent data, int requestCode) {
        Bitmap selectedImageBitmap = null;
        if (data != null) {
            try {
                selectedImageBitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                Log.d(TAG, e.toString());
            }
        }
        if (currentVisibleFragment instanceof LEDDashboardFragment) {
            ((LEDDashboardFragment) currentVisibleFragment).getLedColorsAdapter().displayColorPicker(selectedImageBitmap, requestCode);
        }
    }


    private void onCaptureImageResult(Intent data, int requestCode) {
        if (data != null && data.getExtras() != null) {
            Bitmap selectedImageBitmap = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            if (selectedImageBitmap != null) {
                selectedImageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            }
            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                Log.d(TAG, e.toString());
            }
            if (currentVisibleFragment instanceof LEDDashboardFragment) {
                ((LEDDashboardFragment) currentVisibleFragment).getLedColorsAdapter().displayColorPicker(selectedImageBitmap, requestCode);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MagicSDK.getThingServiceInstance().registerStateChangeListener(
                udid,
                new ThingStateChangeCb() {
                    @Override
                    public void onStateReported(String udid, StateReport state) {
                        Timber.d("Received state report callback for udid = %s", udid);
                        if (isFinishing()) {
                            return;
                        }

                        runOnUiThread(() -> {
                            setState(state);
                            updateUI(state);
                            if (currentVisibleFragment != null) {
                                currentVisibleFragment.updateConnectionState(state.isConnected(), thing.getDeviceName());
                            }
                        });
                    }

                    @Override
                    public void onDeviceConnectionStateChanged(String udid, boolean isOnline, long eventTs) {
                        if (isFinishing()) {
                            return;
                        }
                        runOnUiThread(() ->{
                            currentVisibleFragment.updateConnectionState(isOnline, thing.getDeviceName());
                        });
                    }
                });
    }

    @Override
    protected void onPause() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        super.onPause();
    }

    @Override
    protected void onStop() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        NavigationController.goToUserDashboard(this);
        finish();
    }

    public void displayAlertDialog(String title, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_custom_dialog);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button okbtn = dialog.findViewById(R.id.error_custom_dialog_ok_button);
        TextView titleTv = dialog.findViewById(R.id.error_custom_dialog_title);
        TextView msgTv = dialog.findViewById(R.id.error_custom_dialog_subtitle);
        titleTv.setText(title);
        msgTv.setText(message);

        okbtn.setOnClickListener(v -> {
            dialog.dismiss();
        });

        if (!isFinishing()) {
            dialog.show();
        }
    }

    public void displayWifiAlertDialog(String title, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_custom_dialog);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button okbtn = dialog.findViewById(R.id.error_custom_dialog_ok_button);
        TextView titleTv = dialog.findViewById(R.id.error_custom_dialog_title);
        TextView msgTv = dialog.findViewById(R.id.error_custom_dialog_subtitle);
        titleTv.setText(title);
        msgTv.setText(message);
        dialog.setCancelable(false);
        okbtn.setOnClickListener(v -> {
            if (dialog != null && dialog.isShowing()) {
                NavigationController.goToPhoneWifiSettings(ThingDashboardActivity.this);
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void loadThingDashboard() {
        try {
            Class fragmentClass = getDashboardFragmentClass(thing.getType());
            if (fragmentClass == null) {
                NavigationController.goToUserDashboard(this);
                Log.e(TAG, "No UI defined for the thing type");
                return;
            }
            loadFragment((DashboardFragment) fragmentClass.newInstance());
        } catch (Exception ex) {
            Log.e(TAG, "Unable to create dashboard class object");
            Log.e(TAG, ex.toString());
        }
    }

    private void loadFragment(DashboardFragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.activity_dashboard_fragment_container, fragment);
        transaction.commit();
        currentVisibleFragment = fragment;
    }

    public IoTfyDevice getThing() {
        return thing;
    }

    public void updateUI(StateReport state) {

        if (lastUpdateTs == 0) {
            currentVisibleFragment.updateUI(state);
            return;
        }

        if (lastUpdateTs <= state.getLastStateUpdateTs()) {
            // Check if connected to device via wifi direct
            currentVisibleFragment.updateUI(state);
        }
    }

    public boolean isThingConnected() {
        StateReport currentState = getState();
        if (currentState == null) {
            return false;
        }
        return currentState.isConnected();
    }

    @Override
    public void updateSettings(final JSONObject settingsValueJson) {
        MagicSDK.getThingServiceInstance().updateState(thing.getUdid(), settingsValueJson);
    }

    @Override
    public StateReport getState() {
        if (state == null) {
            return IoTfyDeviceState.getEmptyState(udid);
        }
        return state;
    }

    public void setState(StateReport state) {
        this.state = state;
    }

    @Override
    public String getDisplayLabel() {
        return thing.getDeviceName();
    }

    @Override
    public JSONObject getConfig() {
        return getThing().getConfigJson();
    }

    @Override
    public int getType() {
        return thing.getType();
    }

}

package com.iotfy.sampleapp.things.plug;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.base.ControlThingActivity;
import com.iotfy.sampleapp.models.AdvancedFeature;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VoltageAdapter extends RecyclerView.Adapter<VoltageAdapter.VoltageViewHolder> {
    private static final String TAG = VoltageAdapter.class.getSimpleName();
    private List<AdvancedFeature> advancedFeatureList;
    private ControlThingActivity parentActivity;
    private Map<String, VoltageViewHolder> viewHolderMap;

    public VoltageAdapter(ControlThingActivity parentActivity, List<AdvancedFeature> advancedFeatureList) {
        this.parentActivity = parentActivity;
        this.advancedFeatureList = advancedFeatureList;
        this.viewHolderMap = new HashMap<>();
    }

    @NonNull
    @Override
    public VoltageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_voltage, parent, false);
        return new VoltageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VoltageViewHolder holder, int position) {
        AdvancedFeature advancedFeature = advancedFeatureList.get(position);
        String label = advancedFeature.getLabel();
        label = label.replace(" Cut-off Voltage","");
        holder.voltageTv.setText(label);
        int maxVal = advancedFeature.getDataJson().optJSONObject("values").optInt("max");
        int minVal = advancedFeature.getDataJson().optJSONObject("values").optInt("min");
        int defaultvalue = advancedFeature.getDataJson().optInt("default");

        int value = 0;
        JSONObject currentSettings = parentActivity.getCurrentSettingsJson();
        value = currentSettings.optInt(advancedFeature.getFeatureName(), defaultvalue);

        holder.editIv.setOnClickListener(v -> {
            PlugDashboardFragment plugFrag = (PlugDashboardFragment) parentActivity.getSupportFragmentManager().findFragmentById(R.id.activity_dashboard_fragment_container);
            if (plugFrag != null) {
                plugFrag.voltageChangeDialog(parentActivity, maxVal, minVal, advancedFeature.getFeatureName(), advancedFeature.getLabel(), holder.voltageET.getText().toString());
            }
        });

        holder.voltageET.setText(String.valueOf(value));
        viewHolderMap.put(advancedFeature.getFeatureName(), holder);
    }

    public void disableUI() {
        for (VoltageViewHolder holder : viewHolderMap.values()) {
            holder.editIv.setEnabled(false);
        }
    }

    public void enableUI() {
        for (VoltageViewHolder holder : viewHolderMap.values()) {
            holder.editIv.setEnabled(true);
        }
    }

    public void setVoltageValue(String featureName,int defaultValue) {
        VoltageViewHolder holder = viewHolderMap.get(featureName);
        if (holder != null) {
            JSONObject currentSettings = parentActivity.getCurrentSettingsJson();
            int value = currentSettings.optInt(featureName,defaultValue);
            holder.voltageET.setText(String.valueOf(value));
        }
    }

    @Override
    public int getItemCount() {
        return advancedFeatureList.size();
    }

    static class VoltageViewHolder extends RecyclerView.ViewHolder {

        TextInputEditText voltageET;
        TextView voltageTv;
        ImageView editIv;

        VoltageViewHolder(@NonNull View itemView) {
            super(itemView);
            voltageET = itemView.findViewById(R.id.voltage_editext);
            voltageTv = itemView.findViewById(R.id.voltage_label);
            editIv = itemView.findViewById(R.id.voltage_imageView);
        }
    }
}

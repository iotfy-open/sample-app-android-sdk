package com.iotfy.sampleapp.things.led;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iotfy.magic.common.model.DeviceType;
import com.iotfy.magic.common.model.FeatureControl;
import com.iotfy.magic.common.model.StateReport;
import com.iotfy.sampleapp.R;
import com.iotfy.sampleapp.models.FeatureConfig;
import com.iotfy.sampleapp.models.FeatureConfigBrightness;
import com.iotfy.sampleapp.models.FeatureConfigColor;
import com.iotfy.sampleapp.models.FeatureConfigColorTemp;
import com.iotfy.sampleapp.models.FeatureConfigPower;
import com.iotfy.sampleapp.things.DashboardFragment;
import com.iotfy.sampleapp.things.ThingDashboardActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LEDDashboardFragment extends DashboardFragment {

    private static final String TAG = LEDDashboardFragment.class.getSimpleName();

    private ThingDashboardActivity parentActivity;
    private RecyclerView colorPaletteRv;
    private TextView ledBrightnessValue;
    private TextView ledPowerOffTV;
    private ImageView ledImageView;
    private ImageButton powerBtnIv;
    private LEDColorsAdapter ledColorsAdapter;
    private SeekBar ledIntensityBar;
    private SeekBar ledWhiteGradientBar;
    private RelativeLayout ledFeedbackLayout;
    private List<Integer> valueColorList;
    private List<Integer> displayColorList;
    private boolean hasSingleTemp = false;
    private RelativeLayout colorTempLayout;
    private FeatureConfigPower featureConfigPower;
    private FeatureConfigBrightness featureConfigBrightness;
    private FeatureConfigColor featureConfigColor;
    private FeatureConfigColorTemp featureConfigColorTemp;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = (ThingDashboardActivity) getActivity();
        if (parentActivity == null) {
            return;
        }

        valueColorList = new ArrayList<>();
        displayColorList = new ArrayList<>();
        JSONObject thingConfig = parentActivity.getConfig();

        if (thingConfig != null) {
            Iterator<String> featureIterator = thingConfig.keys();
            while (featureIterator.hasNext()) {
                String feature = featureIterator.next();
                try {
                    FeatureConfig featureConfig = new FeatureConfig(feature, thingConfig.getJSONObject(feature));

                    if (featureConfig.getType().equalsIgnoreCase(FeatureConfigColor.COLOR_TYPE) || featureConfig.getType().equalsIgnoreCase(FeatureConfigColor.SECOND_COLOR_TYPE)) {
                        featureConfigColor = new FeatureConfigColor(feature, thingConfig.getJSONObject(feature));
                    } else if (featureConfig.getType().equalsIgnoreCase(FeatureConfigColorTemp.COLORTEMP_TYPE)) {
                        featureConfigColorTemp = new FeatureConfigColorTemp(feature, thingConfig.getJSONObject(feature));
                        JSONObject colorTemp = featureConfigColorTemp.getValuesJson();
                        int maxvalue = colorTemp.optInt("max", 0);
                        int minvalue = colorTemp.optInt("min", 0);
                        if (minvalue == maxvalue) {
                            hasSingleTemp = true;
                        }
                    } else if (featureConfig.getType().equalsIgnoreCase(FeatureConfigBrightness.BRIGHTNESS_TYPE)) {
                        featureConfigBrightness = new FeatureConfigBrightness(feature, thingConfig.getJSONObject(feature));
                    } else if (featureConfig.getType().equalsIgnoreCase(FeatureConfigPower.POWER_TYPE)) {
                        featureConfigPower = new FeatureConfigPower(feature, thingConfig.getJSONObject(feature));
                    }
                } catch (JSONException e) {
                    Log.w(TAG, e.toString());
                }
            }
        }


        if (hasSingleTemp) {
            valueColorList.add(getResources().getColor(R.color.white));
        }
        valueColorList.add(getResources().getColor(R.color.blue));
        valueColorList.add(getResources().getColor(R.color.green));
        valueColorList.add(getResources().getColor(R.color.yellow));
        valueColorList.add(getResources().getColor(R.color.red));
        valueColorList.add(getResources().getColor(R.color.cyan));
        valueColorList.add(getResources().getColor(R.color.majenta));
        valueColorList.add(getResources().getColor(R.color.violet));
        valueColorList.add(getResources().getColor(R.color.orange));

        if (!hasSingleTemp) {
            valueColorList.add(getResources().getColor(R.color.darkgreen));
        }
        valueColorList.add(getResources().getColor(R.color.black));

        if (hasSingleTemp) {
            displayColorList.add(getResources().getColor(R.color.white));
        }
        displayColorList.add(getResources().getColor(R.color.blue));
        displayColorList.add(getResources().getColor(R.color.green));
        displayColorList.add(getResources().getColor(R.color.yellow));
        displayColorList.add(getResources().getColor(R.color.red));
        displayColorList.add(getResources().getColor(R.color.cyan));
        displayColorList.add(getResources().getColor(R.color.majenta));
        displayColorList.add(getResources().getColor(R.color.violet));
        displayColorList.add(getResources().getColor(R.color.orange));
        if (!hasSingleTemp) {
            displayColorList.add(getResources().getColor(R.color.seagreen));
        }
        displayColorList.add(getResources().getColor(R.color.black));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_led_dashboard, container, false);
        LinearLayout powerBtnLayout = view.findViewById(R.id.fragment_LED_dashboard_PowerLayout_linearLayout);

        colorPaletteRv = view.findViewById(R.id.fragment_LED_dashboard_Function_recyclerView);
        LinearLayout ledColorsLayout = view.findViewById(R.id.fragment_LED_dashboard_ColorsLayout);

        powerBtnIv = view.findViewById(R.id.fragment_LED_dashboard_powerOnOff_imageView);
        ledIntensityBar = view.findViewById(R.id.fragment_LED_dashboard_Intensity_Seekbar);
        ledWhiteGradientBar = view.findViewById(R.id.fragment_LED_dashboard_WhiteGradient_Seekbar);

        ledFeedbackLayout = view.findViewById(R.id.fragment_LED_dashboard_feedbackLayout);
        ledImageView = view.findViewById(R.id.fragment_LED_dashboard_ImageView);
        ledBrightnessValue = view.findViewById(R.id.fragment_LED_dashboard_BrightnessTV);
        ledPowerOffTV = view.findViewById(R.id.fragment_LED_dashboard_powerOff_textView);


        int deviceIconId = R.drawable.ic_frag_dash_led;
        if (parentActivity.getType() == DeviceType.DOWNLIGHT_TYPE) {
            deviceIconId = R.drawable.ic_frag_dash_downlight;
        } else if (parentActivity.getType() == DeviceType.TUBELIGHT_TYPE) {
            deviceIconId = R.drawable.ic_frag_tubelight;
        } else if (parentActivity.getType() == DeviceType.LED_STRIP_TYPE) {
            deviceIconId = R.drawable.ic_thing_dark_19;
        }

        ledImageView.setImageDrawable(ContextCompat.getDrawable(parentActivity, deviceIconId));

        colorTempLayout = view.findViewById(R.id.fragment_LED_dashboard_ColorTemp_linearLayout);
        if (featureConfigColorTemp != null && !hasSingleTemp) {
            colorTempLayout.setVisibility(View.VISIBLE);
        } else {
            colorTempLayout.setVisibility(View.GONE);
        }

        if (featureConfigColor != null) {
            ledColorsLayout.setVisibility(View.VISIBLE);
            colorPaletteRv.setLayoutManager(new GridLayoutManager(getActivity(), 5, GridLayoutManager.VERTICAL, false));
            colorPaletteRv.setHasFixedSize(true);
            ledColorsAdapter = new LEDColorsAdapter(parentActivity, displayColorList, valueColorList,featureConfigColor,featureConfigColorTemp);
            colorPaletteRv.setAdapter(ledColorsAdapter);

            colorPaletteRv.setOnClickListener(view1 -> {
                int color = colorPaletteRv.getSolidColor();
                ledFeedbackLayout.setBackgroundColor(color);
            });
        } else {
            ledColorsLayout.setVisibility(View.GONE);
        }

        final long[] colorTempChangeTimer = {0L};
        ledWhiteGradientBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                long diff = System.currentTimeMillis() - colorTempChangeTimer[0];
                if (diff > 500 && fromUser) {
                    updateWhiteGradient(progress);
                    colorTempChangeTimer[0] = System.currentTimeMillis();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                colorTempChangeTimer[0] = System.currentTimeMillis();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                updateWhiteGradient(seekBar.getProgress());

            }
        });

        ledIntensityBar.setMax(100);
        final long[] brightnessChangeTimer = {0L};
        ledIntensityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                long diff = System.currentTimeMillis() - brightnessChangeTimer[0];
                if (diff > 500 && fromUser) {
                    if (progress < 1) {
                        progress = 1;
                    }
                    updateIntensity(progress);
                    brightnessChangeTimer[0] = System.currentTimeMillis();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                brightnessChangeTimer[0] = System.currentTimeMillis();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int i = seekBar.getProgress();
                if (i < 1) {
                    i = 1;
                    seekBar.setProgress(1);
                }

                updateIntensity(i);
                String setBrightness = parentActivity.getString(R.string.fragment_led_dashboard_brightness) + i;
                ledBrightnessValue.setText(setBrightness);
            }
        });


        powerBtnLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject desiredSettings = new JSONObject();
                    JSONObject currentSettings = parentActivity.getCurrentSettingsJson();

                    if (currentSettings.optInt(FeatureControl.POWER, 0) == 0) {
                        desiredSettings.put(FeatureControl.POWER, 1);
                    } else {
                        desiredSettings.put(FeatureControl.POWER, 0);
                    }
                    parentActivity.updateSettings(desiredSettings);
                } catch (JSONException jex) {
                    Log.w(TAG, jex.toString());
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public LEDColorsAdapter getLedColorsAdapter() {
        return ledColorsAdapter;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateConnectionState(parentActivity.isThingConnected(),
                parentActivity.getDisplayLabel());

        updateUI(parentActivity.getState());
    }


    @Override
    public void updateUI(StateReport state) {

        if (parentActivity == null || state == null) {
            return;
        }

        JSONObject settingsJson;
        try {
            settingsJson = state.getSettings();
        } catch (NullPointerException ex) {
            Log.w(TAG, ex.toString());
            settingsJson = new JSONObject();
        }

        // Power state
        int powerVal = 0;

        Context fragContext = getContext();
        Resources appResources = null;
        if (fragContext != null) {
            appResources = fragContext.getResources();
        } else {
            Log.w(TAG, "Context lost");
        }

        if (appResources == null) {
            return;
        }

        //applying selected color
        int colorVal = 0;
        int rgbValue = 0;
        int red = 0;
        int green = 0;
        int blue = 0;

        powerVal = settingsJson.optInt("pow", 0);

        if (featureConfigColor != null) {
            int color = settingsJson.optInt(FeatureControl.COLOR_RGB);
            String hexcolor = String.format("#%06X", (0xFFFFFF & color));
            red = Integer.valueOf(hexcolor.substring(1, 3), 16);
            green = Integer.valueOf(hexcolor.substring(3, 5), 16);
            blue = Integer.valueOf(hexcolor.substring(5, 7), 16);

            int[] colors = {red, green, blue};
            rgbValue = red + green + blue;
            colorVal = Color.rgb(colors[0], colors[1], colors[2]);
            for (int i = 0; i < valueColorList.size(); i++) {
                if (colorVal == valueColorList.get(i)) {
                    colorVal = displayColorList.get(i);
                }
            }
        }

        GradientDrawable ledBackgroundGradientDrawable = new GradientDrawable();
        if (rgbValue != 0) {
            ledBackgroundGradientDrawable.setColors(new int[]{
                    Color.WHITE, colorVal, Color.WHITE
            });
            ledBackgroundGradientDrawable.setGradientType(GradientDrawable.RADIAL_GRADIENT);
            ledBackgroundGradientDrawable.setGradientRadius(300f);
        } else if (featureConfigColorTemp != null) {
            int ct = settingsJson.optInt(FeatureControl.COLOR_TEMPERATURE);
            if (ct != 0) {
                int temp = colorTemperatureToRGB(ct);
                ledBackgroundGradientDrawable.setColors(new int[]{
                        Color.WHITE, temp, Color.WHITE
                });
                ledBackgroundGradientDrawable.setGradientType(GradientDrawable.RADIAL_GRADIENT);
                ledBackgroundGradientDrawable.setGradientRadius(300f);
            }
        }

        if (powerVal == 0) {
            powerBtnIv.setColorFilter(appResources.getColor(R.color.powerOffColor));
            ledImageView.setVisibility(View.GONE);
            ledPowerOffTV.setVisibility(View.VISIBLE);
            ledFeedbackLayout.setBackgroundColor(appResources.getColor(R.color.feedbackViewOff));
            disableControls();
            updateToolbar(false);
            return;
        } else {
            powerBtnIv.setColorFilter(appResources.getColor(R.color.powerOnColor));
            ledImageView.setVisibility(View.VISIBLE);
            ledPowerOffTV.setVisibility(View.GONE);
            ledFeedbackLayout.setBackground(ledBackgroundGradientDrawable);
            enableControls();
            updateToolbar(true);
        }

        if (featureConfigColorTemp != null) {
            JSONObject colorTemp = null;
            try {
                colorTemp = featureConfigColorTemp.getValuesJson();
                int maxvalue = colorTemp.optInt("max", 0);
                int minvalue = colorTemp.optInt("min", 0);
                if (maxvalue == minvalue) {
                    hasSingleTemp = true;
                    colorTempLayout.setVisibility(View.GONE);
                }
                int ct = settingsJson.optInt(FeatureControl.COLOR_TEMPERATURE, 0);
                if (ct == 0) {
                    ledWhiteGradientBar.setProgress(ct);
                } else {
                    if (maxvalue != minvalue) {
                        int result = ((ct - minvalue) * 100) / (maxvalue - minvalue);
                        int finalcolorTemp = 100 - result;
                        ledWhiteGradientBar.setProgress(finalcolorTemp);
                    }
                }

            } catch (JSONException e) {
                Log.e(TAG, e.toString());
            }
        }

        //brightness/intensity of LED
        if (featureConfigBrightness != null) {
            int intensityVal = settingsJson.optInt(featureConfigBrightness.getName());
            ledIntensityBar.setProgress(intensityVal);
            ledBrightnessValue.setText(getString(R.string.fragment_led_dashboard_brightness) + intensityVal);
        }
    }


    @Override
    public void updateComponentName(String componentIdentifier, String componentName) {

    }

    private void enableControls() {
        ledBrightnessValue.setVisibility(View.VISIBLE);
        ledIntensityBar.setEnabled(true);
        ledWhiteGradientBar.setEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ledWhiteGradientBar.setProgressTintMode(PorterDuff.Mode.SRC_OVER);
            ledWhiteGradientBar.setProgressTintList(getActivity().getColorStateList(R.color.orange));
        } else {
            ledWhiteGradientBar.setProgressBackgroundTintMode(null);
            ledWhiteGradientBar.setProgressBackgroundTintList(null);
            ledWhiteGradientBar.setProgressTintMode(PorterDuff.Mode.SRC_IN);
            ledWhiteGradientBar.setProgressTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));

        }
        if (featureConfigColor != null) {
            ledColorsAdapter.enableUI();
        }
    }

    private void disableControls() {
        ledBrightnessValue.setVisibility(View.GONE);
        ledIntensityBar.setEnabled(false);
        ledWhiteGradientBar.setEnabled(false);
        ledWhiteGradientBar.setProgressTintMode(null);
        ledWhiteGradientBar.setProgressTintList(null);
        if (featureConfigColor != null) {
            ledColorsAdapter.disableUI();
        }
    }

    private void updateIntensity(int i) {
        JSONObject desiredSettings = new JSONObject();
        try {
            desiredSettings.put(FeatureControl.BRIGHTNESS, i);
            parentActivity.updateSettings(desiredSettings);
        } catch (JSONException jex) {
            Log.e(TAG, jex.toString());
        }
    }

    public void updateWhiteGradient(int seekbarValue) {
        JSONObject desiredSettings = new JSONObject();
        try {
            if (featureConfigColorTemp != null) {
                JSONObject colorTemp = featureConfigColorTemp.getValuesJson();
                int maxvalue = colorTemp.optInt("max", 6000);
                int minvalue = colorTemp.optInt("min", 3000);
                int colorTemperature = maxvalue - ((maxvalue - minvalue) * seekbarValue / 100);
                desiredSettings.put(FeatureControl.COLOR_TEMPERATURE, colorTemperature);
            }

            if (featureConfigColor != null) {
                desiredSettings.put(FeatureControl.COLOR_RGB, 0);
            }

            parentActivity.updateSettings(desiredSettings);
        } catch (JSONException jex) {
            Log.e(TAG, jex.toString());
        }
    }

    private int colorTemperatureToRGB(int kelvin) {

        int temp = kelvin / 100;

        double red;
        double green;
        double blue;

        if (temp <= 66) {
            red = 255;
            green = temp;
            green = 99.4708025861 * Math.log(green) - 161.1195681661;
            if (temp <= 19) {
                blue = 0;
            } else {
                blue = temp - 10;
                blue = 138.5177312231 * Math.log(blue) - 305.0447927307;
            }
        } else {
            red = temp - 60;
            red = 329.698727446 * Math.pow(red, -0.1332047592);
            green = temp - 60;
            green = 288.1221695283 * Math.pow(green, -0.0755148492);
            blue = 255;
        }
        return Color.rgb((int) red, (int) green, (int) blue);
    }

    private void updateToolbar(boolean b) {
        if (getView() != null) {
            TextView textView = getView().findViewById(R.id.fragment_dash_actionBar_view_deviceName);
            TextView bottomTextView = getView().findViewById(R.id.fragment_dash_actionBar_view_Label_textView);
            ImageView backArrowImg = getView().findViewById(R.id.fragment_dash_actionBar_relativeLayout);
            ImageView wifiImg = getView().findViewById(R.id.fragment_dash_actionBar_view_wifiDirectMode);
            ImageView internetImg = getView().findViewById(R.id.fragment_dash_actionBar_view_internetMode);
            TextView wifiDirectTextView = getView().findViewById(R.id.fragment_dash_actionbar_textView);
            LinearLayout wifiActionLayout = getView().findViewById(R.id.fragment_dash_actionBar_view_wifiAction_layout);

            if (b) {
                textView.setTextColor(Color.BLACK);
                bottomTextView.setTextColor(Color.BLACK);
                backArrowImg.setColorFilter(Color.BLACK);
                wifiImg.setColorFilter(Color.BLACK);
                internetImg.setColorFilter(Color.BLACK);
                wifiDirectTextView.setTextColor(Color.BLACK);
                wifiActionLayout.setBackground(parentActivity.getResources().getDrawable(R.drawable.rounded_black_outline));

            } else {
                textView.setTextColor(Color.WHITE);
                bottomTextView.setTextColor(Color.WHITE);
                backArrowImg.setColorFilter(Color.WHITE);
                wifiImg.setColorFilter(Color.WHITE);
                internetImg.setColorFilter(Color.WHITE);
                wifiDirectTextView.setTextColor(Color.WHITE);
                wifiActionLayout.setBackground(parentActivity.getResources().getDrawable(R.drawable.rounded_white_outline));

            }
        }
    }
}

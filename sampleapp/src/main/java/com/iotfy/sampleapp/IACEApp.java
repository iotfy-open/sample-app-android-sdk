package com.iotfy.sampleapp;

import android.app.Application;

import com.iotfy.magic.MagicSDK;
import com.iotfy.magic.core.utils.MLog;
import com.iotfy.sampleapp.base.constants.AppIdentity;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;

public class IACEApp extends Application {

    public static final String TAG = IACEApp.class.getSimpleName();
    private static IACEApp instance;

    public static synchronized IACEApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Iconify.with(new FontAwesomeModule());
        MagicSDK.setSDKLogLevel(MLog.LogLevel.DEBUG);
        MagicSDK.init(this, AppIdentity.CLIENT_ID, AppIdentity.CLIENT_KEY);
    }

    @Override
    public void onTerminate() {
        MagicSDK.destroy();
        super.onTerminate();
    }
}

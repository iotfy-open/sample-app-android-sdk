package com.iotfy.sampleapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iotfy.magic.common.model.IoTfyDevice;
import com.iotfy.sampleapp.things.AddDeviceActivity;
import com.iotfy.sampleapp.things.QuickConnectActivity;
import com.iotfy.sampleapp.things.ThingDashboardActivity;
import com.iotfy.sampleapp.user.SplashActivity;
import com.iotfy.sampleapp.user.UserDashboardActivity;
import com.iotfy.sampleapp.wifi.WiFiSelectActivity;

public class NavigationController {

    private static void fireIntent(@NonNull Context ctx, @NonNull Intent intent, @Nullable Bundle bundle) {
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        ctx.startActivity(intent);
    }

    private static void leftToRightAnimation(Activity activity) {
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void goToAppSettings(Context ctx) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", ctx.getPackageName(), null);
        intent.setData(uri);
        ctx.startActivity(intent);
    }

    public static void goToPhoneWifiSettings(Context ctx) {
        ctx.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    public static void goToUserDashboardFromSplash(Activity activity) {
        Intent dashIntent = new Intent(activity, UserDashboardActivity.class);
        dashIntent.putExtra("fromSplash", true);
        dashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        fireIntent(activity, dashIntent, null);
        leftToRightAnimation(activity);
    }

    public static void askWiFiForQuickConnect(Activity activity) {
        Intent wifiSelectIntent = new Intent(activity, WiFiSelectActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(WiFiSelectActivity.INTENT_REASON_ARG, WiFiSelectActivity.QUICK_CONNECT_REASON);
        fireIntent(activity, wifiSelectIntent, bundle);
        leftToRightAnimation(activity);
    }

    public static void goToUserDashboard(Activity activity) {
        Intent dashIntent = new Intent(activity, UserDashboardActivity.class);
        dashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        fireIntent(activity, dashIntent, null);
        leftToRightAnimation(activity);
    }

    public static void goToThingDashboard(Activity activity, IoTfyDevice device) {
        Intent dashIntent = new Intent(activity, ThingDashboardActivity.class);
        dashIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putSerializable("thing", device);
        fireIntent(activity, dashIntent, bundle);
        leftToRightAnimation(activity);
    }

    public static void goToAddDeviceActivity(Activity activity) {
        Intent addDevice = new Intent(activity, AddDeviceActivity.class);
        fireIntent(activity, addDevice, null);
        leftToRightAnimation(activity);
    }

    public static void quickConnectDevices(Activity activity, String userWifiSsid, String userWifiPassword) {
        Intent quickConnectIntent = new Intent(activity, QuickConnectActivity.class);
        Bundle b = new Bundle();
        b.putString(QuickConnectActivity.WIFI_SSID_ARG, userWifiSsid);
        b.putString(QuickConnectActivity.WIFI_PASSWORD_ARG, userWifiPassword);
        fireIntent(activity, quickConnectIntent, b);
        leftToRightAnimation(activity);
    }

    public static void goToSplashActivity(Activity activity) {
        Intent splashIntent = new Intent(activity, SplashActivity.class);
        splashIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        fireIntent(activity, splashIntent, null);
        leftToRightAnimation(activity);
    }
}
